# Disclosure Policies / Relying Party Authorization

This document outlines a proposed solution for implementing the disclosure
policy requirements from the Implementing Acts (IA) 5a to handle authorization
during presentation of credentials.

## Motivation

In this model, the (Q)EAA provider directly defines rules for which RP data
requests are acceptable directly, adhering to the dynamic authorization
framework discussed later. Rather than using an allow list approach, the
authorization is credential based, giving more flexibility to the Issuer to
manage the policy.

The authorization requirements for accessing Personal Identity Data (PID) and
Electronic Attribute Attestation (EAA) data prioritize flexibility and
scalability while addressing the following shortcomings of traditional methods
like allowlists and static trust anchors:

- **Limitations of Allowlists**: Maintaining a comprehensive and updated list of
  entities (e.g., pharmacies) across Europe is impractical, given the large
  number of the Relying Parties and their varying roles.
- **Challenges with Root of Trust Models**: Static root of trust mechanisms
  introduce risks due to inconsistent registry coordination, and are unable to
  adapt to evolving authorization needs across multiple jurisdictions.

To overcome these limitations, the solution advocates for an attribute-based
authorization model that uses a query language like [Digital Credentials Query
Language
(DCQL)](https://openid.net/specs/openid-4-verifiable-presentations-1_0.html#name-digital-credentials-query-l):

- **Attribute-based access control**: The usage of a query language to realize
  authorization policies avoids the pitfalls of static mechanisms by:
    - Ensuring RPs are validated through machine-readable, real-time policies.
    - Allowing issuers to define granular, adaptable rules for data access.
- **Automation and Transparency**: The Wallet processes DCQL based requests
  automatically, providing users with clear insights into how their data will be
  used, while ensuring compliance with issuer-defined policies.

(TODO: When using proximity flow, the query language from MDL spec should be
used.)

## Trust Architecture

- **Authorization Certificate**:
    - An authority (e.g. a banking authority) issues certificates to Relying
      Parties, such as banks.
    - RPs include these certificates in their requests, ensuring only authorized
     RPs access data.

## International interoperability

The format of the following certificates and how to include them in the
presentation request need to be aligned across the EU to ensure interoperability
among the Wallets and the RPs:

- Authorization Certificate

The following can be specified individually by each member state without
impacting interoperability:

- Setting up management for Authorization Certificates

## Out of Scope

The setup and administration of authorities responsible for the discovery of the
disclosure policies and the setup of the authorities issuing the required
credentials are out of scope of this document.

## Overview

```plantuml
digraph G {
    rankdir=LR
    node [shape=box]    

    // Nodes
    tl [label="Trusted List"]
    wrp [label="Wallet-Relying Party"]
    wrp_registrar [label="Wallet-Relying Party Registrar"]
    wallet [label="Wallet"]

    i [label="Issuer"]
    dc [label="Disclosure Policy"]
    auth [label="Authority for an eco system"]
        
    ac [label="Authorization Certificate", fillcolor=yellow, style=filled]
    vc [label="Verifiable Credential", fillcolor=yellow, style=filled]
    vp [label="Verifiable Presentation"  fillcolor=yellow, style=filled]

    // Edges
    wrp -> wrp_registrar [label="onboarded"]
    wrp_registrar -> tl [label="published"]
    vp -> wrp [label="presented to"]
    wallet -> vp [label="creates"]

    i -> vc [label="issues"]
    vc -> wallet [label="issued for"]
    vc -> dc [label="references"]
    dc -> auth [label="references as authority"]
    auth -> ac [label="issues"]
    ac -> wrp [label="issued for"]
    ac -> dc [label="required for"]
}
```

## Authorization Certificate

The Authorization Certificate is a JWS/COSE_Signature that is used for the
attribute based access control by the RP and is bound to the [Access
Certificate](../../flows/Wallet-Relying-Party-Authentication.md#access-certificate).

### JWS

**`typ`**: REQUIRED, should be set to `auth-dcql`. Value is not defined in the
[IANA media
types](https://www.iana.org/assignments/media-types/media-types.xhtml) registry
yet.

**`iss`**: REQUIRED, the distinguished name of the issuer of the Authorization
Certificate, which should be the authority that issues the attribute based
access control attestation. When the Wallet receives an authorization
certificate, it has to make sure that the issuer is matching with the defined
`root_of_trust` by the credential's issuer.

**`sub`**: REQUIRED, the subject of the Authorization Certificate, which should
be the distinguished name of the RP.

**`status`**: REQUIRED, reference to the status management, the [token status
list](https://datatracker.ietf.org/doc/draft-ietf-oauth-status-list/) is used.

**`iat`**: REQUIRED, the time the Authorization Certificate was issued.

**`exp`**: OPTIONAL, the expiration time of the Authorization Certificate.

**`cnf`**: OPTIONAL, the confirmation method to bind an Authorization
Certificate to a specific Access Certificate. The [Certificate Thumbprint
Confirmation
Method](https://www.rfc-editor.org/rfc/rfc8705.html#name-jwt-certificate-thumbprint-)
is used to bind the Authorization Certificate to one Access Certificate. Since
the access certificate is a X509 based one, this seems to be the only
standardize way to bind it according to the published [JWT conforming
methods](https://www.iana.org/assignments/jwt/jwt.xhtml#confirmation-methods).

```json
{
  "x5c": ['dddd'], // chain of trust, first entry is the regulator, e.g. BMI
  "typ": "auth-dcql",
}
.
{  
  "iss": "CN=BMI, O=BMI, C=DE", // should be the distinguished name of the issuer
  "sub": "CN=SPRIND GmbH, O=SPRIND GmbH, C=DE", //should be the distinguished name of the RP Access Certificate
  "iat": 1683000000,
  "isAgency": true,  
  "status": {
    "status_list": {
      "idx": 0,
      "uri": "https://example.com/statuslists/1"
    }
  },
  "cnf": {
    "x5t#S256": "bwcK0esc3ACC3DB2Y5_lESsXE8o9ltc05O89jdN-dg2" //fingerprint to the RP access certificate    
  },
}
```

### COSE_Signature

The same attributes as in the JWS are used.

**`cnf`**: The Certificate Thumbprint Confirmation Method is not one of the
[registered confirmation
methods](https://www.iana.org/assignments/cwt/cwt.xhtml), but it is used to bind
the Authorization Certificate to the Access Certificate. The `x5t#S256` is the
used method name.

## Disclosure policy

Each credential has it's own policy set for authorization. A RP should be able
to register the mapping of the credentials it is issuing and policies in the RP
Registrar database so it can be discovered and fetched by others.

The whitelist and rootOfTrustList approach from the IA 5a are not recommended
here because of inefficiency. The whitelist would need a constant update when
new relying parties should be added or removed. And the rootOfTrustList will be
quite insecure since you can only limit it down to one specific registrar in a
member state, but not go into more detail.

While embedding the policy has the advantage of being self-contained, the issuer
is not able to update the policy without reissuing the credential. Because of
this, the policy should be linked and then cached by the Wallet.

### Policies

The authorization methods will be defined in the `authz` to support alternative
approaches in the future. The issuer should not add more than one method to the
`authz` object since it is not clear which one should be used, what the priority
is and what to do in case of a conflict.

**`allowlist`**: a list of Relying Parties identifiers that are authorized to
request this credential. This approach is called `Authorized relying party only
policy` in the Implementing Acts. For the unique identifier the distinguished
name of the RP is used.

**`rootOfTrust`**: the certificate of a RP must be derived from a list of
specific root certificates. In the Implementing Acts it's called `Specific root
of trust`. The value has to be the distinguished name of the root certificate.

**`attribute_based_access_control`**: a attribute based access control approach
where the issuer is able to define the conditions of credentials the Relying
Party has to pass to the Wallet to be authorized. The Dynamic Credentials Query
Language (DCQL) is used to describe the requirements and allow the Wallet to
automatically compare this against the presented credentials of the Relying
Party.

- The `credentials` parameter represents a list of [credential
queries](https://openid.net/specs/openid-4-verifiable-presentations-1_0.html#name-credential-query).
This functionality is enhanced by the `root_of_trust` property, which specifies
a list of issuers or trust anchors, represented by the distinguished name, that
the credential's issuer must be based upon. For example, in Germany, the Federal
Financial Supervisory Authority (BaFin) can be designated as the root of trust
for the `isBank` attribute, ensuring that only entities recognized as banks can
issue this attestation to Relying Parties classified as banks.
- The `credential_sets` parameter represents a lists of [credential set
queries](https://openid.net/specs/openid-4-verifiable-presentations-1_0.html#name-credential-set-query),
allowing to define multiple conditions for authorization. In the example below a
RP is authorized to fetch the credential when it has either an attestation for
being a bank or a ministry.

```json
{
  "attribute_based_access_control":{
    "credentials":[
      {
        "root_of_trust":[
          "C=BAFIN"
        ],
        "id":"bank",
        "format":"jws", //needs to be set to jws since authorization certificates are jws        
        "claims":[
          {
            "path":"isBank",
            "values":[
              true
            ]
          }
        ]
      },
      {
        "root_of_trust":[
          "C=BUND"
        ],
        "id":"ministry",
        "format":"jws",        
        "claims":[
          {
            "path":"ministry",
            "values":[
              true
            ]
          }
        ]
      }
    ],
    "credential_sets":[
      {
        "options":[
          [
            "bank"
          ],
          [
            "ministry"
          ]
        ]
      }
    ]
  },
  "claims":{
    "org.iso.18013.5.1":{
      "given_name":{
        "display":[
          {
            "name":"Given Name",
            "locale":"en-US"
          },
          {
            "name": "Vorname",
            "locale":"de-DE"
          }
        ]
      },
      "family_name":{
        "display":[
          {
            "name":"Surname",
            "locale":"en-US"
          }
        ]
      },
      "birth_date":{
        "mandatory":true
      }
    }
  }
}
```

### Providing approaches

To fetch the policy, the issuer is able to link them via the credential format
or the transport method.

#### Credential format

For SD-JWT VC, the policy can be referenced in the [SD-JWT VC Type
Metadata](https://www.ietf.org/archive/id/draft-ietf-oauth-sd-jwt-vc-08.html#name-sd-jwt-vc-type-metadata).
To protect the integrity of the policy, the issuer can add an integrity check
and reference it in the metadata.

```json
{
  "vct":"https://example.com/education_credential",
  "name":"Education Credential - Preliminary Version",
  "description":"This is our development version of the education credential. Don't panic.",
  "extends":"https://galaxy.example.com/galactic-education-credential-0.9",
  "extends#integrity":"sha256-9cLlJNXN-TsMk-PmKjZ5t0WRL5ca_xGgX3c1VLmXfh-WRL5",
  "schema_uri":"https://exampleuniversity.com/public/credential-schema-0.9",
  "schema_uri#integrity":"sha256-o984vn819a48ui1llkwPmKjZ5t0WRL5ca_xGgX3c1VLmXfh",
  "authz": "http://example.com/policy/education_credential",
  "authz#integrity": "sha256-o984vn819a48ui1llkwPmKjZ5t0WRL5ca_xGgX3c1VLmXfh"
}
```

#### Transport method

The policy can be linked in the issuer metadata:

```json
{
  "credential_configurations_supported": {
    "SD_JWT_VC_example_in_OpenID4VCI": {
      "format": "dc+sd-jwt",
      "scope": "SD_JWT_VC_example_in_OpenID4VCI",
      "cryptographic_binding_methods_supported": ["jwk"],
      "credential_signing_alg_values_supported": ["ES256"],
      "vct": "SD_JWT_VC_example_in_OpenID4VCI",
      "authz": {}, //policy is embedded or linked here
  }
}
```

Instead of publishing the rules directly into the issuer vc metadata, the rules
can also be linked like:

```json
{
  "authz": {
      "reference": "http://example.com/policy/education_credential",
      "reference#integrity": "sha256-o984vn819a48ui1llkwPmKjZ5t0WRL5ca_xGgX3c1VLmXfh"
  }
}
```

### Privacy Considerations

Since the policy is linked and needs to be fetched, there is a minimal risk of
monitoring:

- The policy for it is placed into the general issuer metadata at
  `/.well-known/openid-credential-issuer` that includes all credentials the
  issuer offers.
- The Wallet is able to cache these information locally and needs to fetch the
  endpoint just to get periodic updates. This could also be the case to sync
  information like the
  [display](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html#name-credential-issuer-metadata:~:text=of%20this%20specification.-,display,-%3A%20OPTIONAL.%20Array%20of)
  data.
- The request of the endpoint is without any kind of authentication so the
  issuer only receives the IP address. The Wallet should not insert any user
  agent information that allow the issuer to identify the holder.

A malicious issuer could use a unique domain like `4728202.example.com` so each
credential issued is using a unique sub domain. To eliminate this risk, all
domains need to be registered or make public available, e.g. via the RP
Registrar to avoid such a behavior and to allow the Relying Parties to discover
the authentication rules for the credentials offered by an Issuer. The same
attack vector exists when using status management like CRL or bitstring based
approaches, where each credential gets a unique url.
