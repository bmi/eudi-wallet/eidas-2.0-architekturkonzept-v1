<!-- PDF-show
\newpage
-->

# Wallet Function: PID Issuance and Presentation

The PID is an attestation of person identity data and an electronic identity as
defined in eIDAS. It can be used by a user to identify themselves at a Relying
Party. This section discusses options to present a PID in a remote transaction
(incl. authentication).

## Requirements

The following additional requirements apply to the PID solutions:

- the PID and its complete lifecycle, including its issuance and presentation
  processes, shall fulfill the requirements defined for Level of Assurance High
- the PIDs in the German Wallet implementation and their use as an electronic
  identity shall be based on the German eID

## Reference Design Proposals

This section lays out different design options for implementing the remote
(i.e., online) issuance and presentation using the German eID system.

Note: The current architecture proposal aims at covering the potential solution
space based on existing solutions in Germany and the EU as well as international
developments in order to provide the basis for a comprehensive analysis and
discussion. The objective is to find the best solution based on an assessment
from different angles, such as security, privacy, complexity, potential user
reach, and user experience. The number of options will be reduced in the course
of that process.

The PID Provider obtains the EUDIW user's identity data using the German eID
system. It then creates a PID credential in either ISO mdoc or SD-JWT VC format.
From a high level view, two options exist to transmit a PID credential: The PID
Provider either provides the credential directly (Server Retrieval) or
indirectly (Device Retrieval) to the Relying Party that requested for the PID as
shown in Figure 2. A Relying Party trusts the PID data if it trusts the PID
Provider and its issuance and presentation process.

![Depicting data flows and trust relationships.](../../figures/figure2.svg)

<!-- PDF-hide-start -->
*Figure 2: Architecture overview.*
<!-- PDF-hide-end -->

The Server Retrieval options are not pursued for the German EUDI Wallet
architecture as this would allow the PID provider to monitor where the user
submits their PID and therefore contradict essential privacy requirements.

The possible architecture designs for Device Retrieval are further distinguished
by the following factors:

- **Trust Anchor (Unforgeability)**: Two mechanisms may be used to
  cryptographically ensure the authenticity and integrity of the PID:
    - Authenticated Channel - The data is protected with an HMAC and may be
  encrypted using a symmetric secret specific to a certain transaction with a
  Relying Party. The secret is determined through key agreement (ECDH) based on
  an ephemeral key of the Relying Party. The secure channel serves as replay
  protection and binds the data to a specific transaction. The approach requires
  the authoritative source to create the secure channel or a party that has a
  similar security and trust level (e.g., a trusted applet residing in a
  certified Secure Element). This approach is used by the German eID system.

    - Signature - The data is protected by a signature created by the
  PID-Provider. The PID is bound to a certain key managed by the Wallet. This
  key is used to sign a presentation of the PID to the RP, which serves as
  cloning protection and binds the presentation to a certain transaction. Since
  the data is signed, it provides non-repudiation but eliminates proper
  plausible deniability for the user to an uninvolved party. This approach is
  used today, e.g., for QES.

- **Trust Anchor (User Binding)**: Different mechanisms may be used to securely
  bind the PID to the user as identity holder:
    - eID-Card - Secure authentication is based on the German eID-Card
    - Cloud Support - Secure authentication is based on a trust anchor in the
    cloud.
    - SE Smartphone - Secure authentication is based on a SE within the
      smartphone

Note 1: All architecture designs allow for the request and presentation of PID
or one or more (Q)EAAs or both in one session by the Relying Party and the
Wallet.

All architecture designs allow for the following technical standards:

- [OpenID for Verifiable Credentials](https://openid.net/sg/openid4vc/) protocol
  family
    - OpenID4VCI for Issuance of the PID
    - OpenID4VP for Presentation of the PID and optionally other (Q)EAAs
- PID Credential Formats
    - IETF [SD-JWT
    VC](https://datatracker.ietf.org/doc/html/draft-ietf-oauth-sd-jwt-vc)
    - ISO mdoc (as defined in ISO 18013-5)

Additionally, an alternative implementation based on the RestAPI protocol
according to ISO/IEC 23220-4 is shown for Option B below.

All PID options allow the issuance of either SD-JWT VC or ISO mdoc or even a
combination of both. For the sake of brevity, not all PID option flows include
both credential format options.

Based on these architecture choices, Figure 3 gives an overview of the solution
space.

![Overview solution options](../../figures/PID_Options_v5.png)

<!-- PDF-hide-start -->
*Figure 3: Overview of solution options.*
<!-- PDF-hide-end -->

The different solution options are explained in the following and contain a
technical description including the cryptographic assets and sequence diagrams.
Additionally, each option is linked to a user journey that shows the various
flows from a user perspective as lo-fi wireframe screen sequences. All screens
of the User Journey are labeled with a specific name. Within the associated
sequence diagram, green hexagonal markers labeled "Screen: screen_name" are
placed to indicate the corresponding screen in that sequence. The Lo-fi
wireframes primarily serve to illustrate the architecture flow from the UX/UI
perspective and to show how an implementation could look from the UX/UI
perspective. However, it should be kept in mind that the actual realization and
implementation of the UX/UI is the responsibility of the wallet providers and
the relying parties.

<!-- PDF-show
\newpage
-->

### PID Provider Authenticated Channel

**Authenticated Channel with eID-Card (B)** - The authentication of the identity
holder for any presentation of the PID is based on the German eID. The PID is
created by the PID Provider on demand and presented via the Wallet to the RP
within a MAC-authenticated secure channel.

- [User Journey: PID Presentation - Authenticated Channel - eID
  Card](../../user_journeys/PID-AuthenticatedChannel-eIDcard-presentation.png)
- [Technical Description and Sequence
  Diagrams](../../flows/PID-AuthenticatedChannel-eIDcard.md)
- Alternative implementation using the RestAPI protocol according to ISO/IEC
  23220-4: [Technical Description and Sequence
  Diagrams](../../flows/PID-AuthenticatedChannel-eIDcard-RESTAPI.md)

**Authenticated Channel with Cloud Support (B')** - A "seed credential" is
derived by the PID-Provider (e.g., using the German eID) and stored in the
Wallet signed by the PID Provider for a limited time-period. The PID is created
by the PID Provider out of the seed credential on demand and presented via the
Wallet to the RP within a MAC-authenticated secure channel. Therefore, the
holder authenticates to the PID-Provider based on the security functions of the
smartphone and a secure evaluation of the PIN by the PID-Provider in the cloud.

- [User Journey: Wallet Activation - Authenticated Channel -
  Cloud](../../user_journeys/PID-AuthenticatedChannel-cloud-initialization.png)
- [User Journey: PID Issuance - Authenticated Channel -
  Cloud](../../user_journeys/PID-AuthenticatedChannel-cloud-issuance.png)
- [User Journey: PID Presentation - Authenticated Channel -
  Cloud](../../user_journeys/PID-AuthenticatedChannel-cloud-presentation.png)
- [Technical Description and Sequence
  Diagrams](../../flows/PID-AuthenticatedChannel-cloud.md)

**Authenticated Channel with SE Smartphone (D)** - The PID data is issued by the
PID Provider into a (Java Card) Applet of the Wallet Provider in the Secure
Element on the user's device (e.g., using the "Online-Ausweisfunktion"). The PID
authorizes a Wallet-controlled key to present the PID to RPs through a
MAC-authenticated secure channel. Therefore, the holder authenticates to the
Secure Element based on the secure evaluation of authentication by the Secure
Element of the smartphone.

- [User Journey: Wallet Activation - Authenticated Channel - Secure
  Element](../../user_journeys/PID-AuthenticatedChannel-secureElement-initialization.png)
- [User Journey: PID Issuance - Authenticated Channel - Secure
  Element](../../user_journeys/PID-AuthenticatedChannel-secureElement-issuance.png)
- [User Journey: PID Presentation - Authenticated Channel - Secure
  Element](../../user_journeys/PID-AuthenticatedChannel-secureElement-presentation.png)
- [Technical Description and Sequence
  Diagrams](../../flows/PID-AuthenticatedChannel-secureElement.md)

### PID Provider Signed Credential

Options for PID Provider signed credentials leverage the concept of a Wallet
Secure Cryptographic Device (WSCD). This is an abstraction that can be
implemented in many ways, including secure elements residing in a mobile device
(SE), external tokens (such as eID cards or FIDO authenticators), an applet on a
eUICC, remote services (based on a remote HSM), and combinations of those
options.

**Signed Credential with eID-Card (C'')** - The authentication of the identity
holder for any presentation of the PID is based on the "Online-Ausweisfunktion".
The PID is created by the PID Provider on demand as a PID Provider signed
credential and presented via the Wallet to the RP.

- User Journey: PID Presentation - TBD
- [Technical Description and Sequence
  Diagrams](../../flows/PID-IssuerSigned-eIDcard.md)

**Signed Credential with Cloud Support (C')** - A credential is derived by the
PID-Provider (e.g. using the "Online-Ausweisfunktion") and stored in the Wallet
as a credential signed by the PID Provider. The cloud/remote HSM acts as a WSCD
that controls the keys used for presentation of the PID. The Wallet presents the
Issuer-Signed PID credential to the RP and the key binding is performed by the
cloud HSM. Therefore, the user authenticates to the cloud-based key store based
on the security functions of the smartphone and a secure evaluation of the PIN
by the cloud based key store.

- [User Journey: Wallet Activation - Issuer Signed -
  Cloud](../../user_journeys/PID-IssuerSigned-cloud-initialization.png)
- [User Journey: PID Issuance - Issuer Signed -
  Cloud](../../user_journeys/PID-IssuerSigned-cloud-issuance.png)
- [User Journey: PID Presentation - Issuer Signed -
  Cloud](../../user_journeys/PID-IssuerSigned-cloud-presentation.png)
- [Technical Description and Sequence
  Diagrams](../../flows/PID-IssuerSigned-cloud.md)

**Signed Credential with SE Smartphone (C)** - A credential is derived by the
PID-Provider (e.g., using the "Online-Ausweisfunktion") and stored in the Wallet
as a credential signed by the PID Provider. The Secure Element acts as a WSCD
that controls the keys used for presentation of the PID. The Wallet presents the
Issuer-Signed PID credential to the RP and the key binding is performed by the
Secure Element. Therefore, the user authenticates to the secure key store of the
smartphone based on the secure evaluation of authentication by security
functions of the smartphone.

- [User Journey: PID Issuance - Issuer
  Signed](../../user_journeys/PID-IssuerSigned-issuance.png)
- [User Journey: PID Presentation - Issuer
  Signed](../../user_journeys/PID-IssuerSigned-presentation.png)
- [General Technical Description of Signature-based
Credentials](../../flows/PID-IssuerSigned.md) (within the general description,
the WSCD is realized with a SE in the smartphone and user authentication is
realized with a PIN evaluated in the SE in the smartphone)

## Preliminary Assessment and Comparison of PID Design Options

The architectural options presented do not claim to represent a perfect solution
for all problems in the environment of an EUDI wallet. All options have their
own advantages and disadvantages in security, privacy and usability and
scalability. Some options are more suitable for simple (Q)EAAs than others, but
in principle there are additional legal requirements for the use and
consideration of the PID that make a different or more complex solution
necessary for the identification/authentication on LoA high. In the current
(early) phase of the project, the options are therefore to present a basis for
discussion and further analysis, including the development of new alternatives.
A complete assessment (based on BSI-TR 03107) of the solutions regarding the
level of assurance, will be possible when the design/architecture is finalized.
For now, several assumptions are made e.g. regarding a proper implementation and
a certification of the used hardware etc.

<!-- PDF-show
\blandscape
-->

<table>
    <thead>
        <tr>
            <th></th>
            <th colspan=3 align="center">Authenticated Channel</th>
            <th colspan=2 align="center">Signed Credential</th>
        </tr>
        <tr>
            <th></th>
            <th align="center">eID-Card</th>
            <th align="center">Cloud Support</th>
            <th align="center">SE Smartphone</th>
            <th align="center">Cloud Support</th>
            <th align="center">SE Smartphone</th>
        </tr>
        <tr>
            <th></th>
            <th align="center">B</th>
            <th align="center">B'</th>
            <th align="center">D</th>
            <th align="center">C'</th>
            <th align="center">C</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan=6 bgcolor=#ccc>Privacy Considerations</td>
        </tr>
        <tr>
            <td> Unobservability</td>
            <td>The PID provider is involved in the presentation and can learn the time and frequency of the presentation as well as the attriputes presented. The Wallet provider is not involved in the presentation.</td>
            <td>The PID provider is involved in the presentation and can learn the timing and frequency of presentations. The Wallet provider is not involved in the presentation.</td>
            <td> PID provider and Wallet provider are not involved in the presentation.</td>
            <td> PID provider and Wallet provider are not involved in the presentation.
            The cloud key store provider cannot assert whether the usage of a key belongs to a PID presentation.  </td>
            <td> PID provider and Wallet provider are not involved in the presentation.</td>
        </tr>
         <tr>
            <td> Unlinkability</td>
            <td>
            RP: An RP cannot assign two presentations to the same user (ephemeral keys).
            RP+RP: see RP.
            PP+RP: Could assign issuance and presentation transactions to the same user if PP logs PID presentation keys.</td>
            <td>
            RP: An RP cannot assign two presentations to the same user (ephemeral keys).
            RP+RP: see RP.
            PP+RP: Could assign issuance and presentation transactions to the same user if PP logs PID presentation keys.</td>
            <td>
            RP: An RP cannot assign two presentations to the same user (ephemeral keys).
            RP+RP: see RP.
            PP+RP: Could assign issuance and presentation transactions to the same user if PP logs PID presentation keys (could be prevented if group keys are set for multiple users).</td>
            <td>
            RP: An RP cannot assign two presentations to the same user (ephemeral keys).
            RP+RP: see RP.
            PP+RP: Could assign issuance and presentation transactions to the same user if PP logs PID presentation keys.</td>
            <td>
            RP: An RP cannot assign two presentations to the same user (ephemeral keys).
            RP+RP: see RP.
            PP+RP: Could assign issuance and presentation transactions to the same user if PP logs PID presentation keys.</td>
        </tr>
        <tr>
            <td> Selective Disclosure</td>
            <td>Achieved through logic in SE of eID-Card.</td>
            <td>Realized with salted hashes.</td>
            <td>Achieved through logic in SE of smartphone.</td>
            <td>Realized with salted hashes.</td>
            <td>Realized with salted hashes.</td>
        </tr>
         <tr>
            <td> Repudiation</td>
            <td>The authenticity of the PID cannot be proven to third parties. PID is not signed. Ephemeral keys are used for authenticated channel.</td>
            <td>The authenticity of the PID cannot be proven to third parties. PID is not signed. Ephemeral keys are used for authenticated channel. </td>
            <td>The authenticity of the PID cannot be proven to third parties. PID is not signed. Ephemeral keys are used for authenticated channel.</td>
            <td>The authenticity of the PID can be proven to third parties. PID is signed.</td>
            <td>The authenticity of the PID can be proven to third parties. PID is signed.</td>
        </tr>
        <tr>
            <td colspan=6 bgcolor=#ccc>Security Considerations</td>
        </tr>
        <tr>
            <td> LoA</td>
            <td>By leveraging the security functionality of the eID-Card and PID provider, PID retrieval could be sufficient for LoA high.</td>
            <td>TBD</td>
            <td>By having most security functionality within secure hardware, this option may be able to achieve LoA high, depending on the degree to which the secure hardware can be protected.</td>
            <td>TBD</td>
            <td>By having most security functionality within secure hardware, this option may be able to achieve LoA high, depending on the degree to which the secure hardware can be protected.</td>
        </tr>
         <tr>
            <td> Unforgeability</td>
            <td>PP authenticated channel (ECDH+HMAC)</td>
            <td>PP authenticated channel (ECDH+HMAC)</td>
            <td>PP authenticated channel (ECDH+HMAC)</td>
            <td>PP signed credential</td>
            <td>PP signed credential</td>
        </tr>
         <tr>
            <td> Freshness</td>
            <td>Dynamic authentication via challenge Response (OpenID4VC)</td>
            <td>Dynamic authentication via challenge Response (OpenID4VC)</td>
            <td>Dynamic authentication via challenge Response (OpenID4VC)</td>
            <td>Dynamic authentication via challenge Response (OpenID4VC)</td>
            <td>Dynamic authentication via challenge Response (OpenID4VC)</td>
        </tr>
         <tr>
            <td> User Binding</td>
            <td>2FA of eID-Card via knowledge (PIN, validated in SE of eID-Card) and possession (key, SE of eID-Card)</td>
            <td>2FA via knowledge (PIN, secure cloud validation) and possession (smartphone-bound key, TEE/TRH/SE)</td>
            <td>2FA via knowledge (PIN, validated in SE of smartphone) and possession (smartphone-bound key, SE)</td>
            <td>2FA via knowledge (PIN, secure cloud validation) and possession (smartphone-bound key, TEE/TRH/SE)</td>
            <td>2FA via knowledge (PIN, validated in SE of smartphone) and possession (smartphone-bound key, SE)</td>
        </tr>
          <tr>
            <td colspan=6 bgcolor=#ccc>Usability Considerations</td>
        </tr>
        <tr>
            <td> External token required for presentation by the user</td>
            <td>eID-Card required</td>
            <td>no</td>
            <td>no</td>
            <td>no</td>
            <td>no</td>
        </tr>
        <tr>
            <td>Combined presentation of PID and (Q)EAA</td>
            <td>yes</td>
            <td>yes</td>
            <td>yes</td>
            <td>yes</td>
            <td>yes</td>
        </tr>
          <tr>
            <td colspan=6 bgcolor=#ccc>Scalability Considerations</td>
        </tr>
        <tr>
            <td> Minimum Device Requirements</td>
            <td>Any NFC equipped device. Platform mechanisms (e.g., Android Key Attestation, iOS App Attestation) for wallet attestations. </td>
            <td>Any NFC equipped device. Platform mechanisms (e.g., Android Key Attestation, iOS App Attestation) for wallet attestations. </td>
            <td>Any NFC equipped device with programmable secure element or eUICC resistant to attack potential high. Platform mechanisms (e.g., Android Key Attestation, iOS App Attestation) for wallet attestations.</td>
            <td>Any NFC equipped device. Platform mechanisms (e.g., Android Key Attestation, iOS App Attestation) for wallet attestations.</td>
            <td>Any NFC equipped device with programmable secure element or eUICC or OEM key store resistant to attack potential high. Platform mechanisms (e.g., Android Key Attestation, iOS App Attestation) for wallet attestations.</td>
        </tr>
          <tr>
            <td> Interoperability Wallet & RP</td>
            <td>The proposed design requires additional algorithms for key agreement and message protection in the ARF (Authenticated Channel).</td>
            <td>The proposed design requires additional algorithms for key agreement and message protection in the ARF (Authenticated Channel). </td>
            <td>The proposed design requires additional algorithms for key agreement and message protection in the ARF (Authenticated Channel).</td>
            <td>This option is lined up with the current ARF.</td>
            <td>This option is lined up with the current ARF.</td>
        </tr>
          <tr>
            <td> Offline support</td>
            <td>No (eID-Service required)</td>
            <td>No (cloud support required)</td>
            <td>Yes</td>
            <td>No (cloud support required)</td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>Cloud service with high availability required for presentation</td>
            <td>eID-Service for eID-Card & PID-Provider for ad hoc issuance</td>
            <td>PID-Provider for ad hoc issuance</td>
            <td>No</td>
            <td>Cloud key store</td>
            <td>No</td>
        </tr>
    </tbody>
</table>

<!-- PDF-show
\elandscape
-->

## Considerations on Performance, Availability, etc. of the individual components

Depending on the chosen architecture, there are a wide variety of components
that all have different requirements of availability or performance and for
which scalability must be considered. The following list is intended to provide
a brief overview of the relevant components but is not exhaustive.

- PID Provider (including eID-Server, HSM)
- Wallet instance
- Wallet Secure Cryptographic Device (remote or on the device)
- Wallet Backend (including HSM)

Backend components might be required, which has a significant influence on the
various aspects covered in this section.

### Availability

It can be assumed that the wallet can be in different stages of use, which have
different requirements for the components:

- During issuance of Credentials, PID provider and the WSCD must be available.
  When the WSCD is not part of the Smartphone of the holder/user, the remote
  WSCD service must be available too.
- During presentation of Credentials, the WSCD and, only in some cases, the PID
provider are required. Therefore, the estimation of the scalability of the PID
provider depends directly on the choice of the actual infrastructure and
architecture of a wallet solution: required availability might be temporary or
recurring.

The wallet provider (and its key management hardware) must be available during
the onboarding of a wallet. Depending on the choice of revocation mechanism,
later retrievals can be possible or necessary, which would also have an impact
on the availability requirements of this component.

### Scalability

For the scalability of the PID provider's eID service, the existing eID systems
can be used. Reading the data would not be a problem, assuming the eID service
has sufficient resources available.

<!-- PDF-show
\blandscape
-->

<table>
    <tr>
        <th></th>
        <th>Options that use Secure Element on the Smartphone (D+C)</th>
        <th>Option with on demand PID issuance (B+C'')</th>
        <th>Options with Could Support (B'+C')</th>
    </tr>
    <tr>
        <td>Performance</td>
        <td>Generally speaking, background communication is only needed for the provisioning and personalization of the PID, therefore short down times for maintenance and similar events will only affect relatively few users.</td>
        <td>The PID Provider is contacted during presentation and must therefore be available at all times and be able to withstand unforseeable load peaks.</td>
        <td>The wallet backend is accessed during every presentation and must therefore be available at all times and be able to withstand unforeseeable load peaks.</td>
    </tr>
    <tr>
        <td>Scalability</td>
        <td>A lot of unknowns when it comes to provisioning and personalization scalability.</td>
        <td>Backend communication through one or more PID providers would be scalable.</td>
        <td>Need to consider how to scale HSM communications.</td>
    </tr>
</table>

<!-- PDF-show
\elandscape
-->

## Hardware security considerations

The options that use a PID provider authenticated channel use signature keys to
authenticate the ephemeral keys for the authenticated channel.

Option B and B' use a signature key for the authenticity of the ephemeral keys
and thus also for the authenticity of the PIDs issued ad hoc. The signature key
for B and B' is located, for example, in an HSM in the environment of the PID
provider. If the signature key were compromised, an attacker could theoretically
authenticate any PIDs. The signature key would have to be revoked. However, no
PIDs would have to be revoked, as no permanently valid PIDs are issued.

Option D presumably uses a group key such as the eID card that is stored in the
applet in the SE of the smartphone in the user's environment. If the group key
is compromised, an attacker could theoretically authenticate any PIDs. The group
key would have to be revoked. All PIDs that use the same group key would no
longer work. However, it is not known which issued PIDs are affected, resulting
in a complex revocation scenario.

The options that use signed credentials use a signature key for signing that is
located for all options, e.g. in an HSM in the PID provider's environment.

If the signature key is compromised, an attacker could theoretically
authenticate any PIDs. The PIDs already issued would have to be revoked. The
impact could be reduced if the diversity of the signature keys were increased,
e.g., for short-lived and long-lived signed credentials.

As there is currently no publicly available information on the breach of keys in
Common Criteria certified SEs or HSMs, it can be assumed that the probability of
these devices being compromised is very low. In any case, a proper certification
of the secure hardware is mandatory to ensure it is tamper and duplication proof
against a high attack potential. Adhering to using only certified Secure
Elements from trustworthy (European) hardware manufacturers reduces the risk of
compromise even further. Usage of remote/cloud HSMs would further require the
security environment to be certified according to ISO 27001. The SE chip/HSM
production can only be verified through third party certification, as such the
security may be considered less reliable than for the smart card. Whether this
"risk" can be accepted for LoA “high” has not been thoroughly evaluated yet.

Furthermore, the data authenticity during transmission from the eID card to the
PID Provider during issuance is done via the German eID for all options. We are
confident that using the German eID (notified for LoA “high”) raises only
acceptable risks for all use-cases. Consistent testing, certification and
mandatory supervision of the chip production under the Federal Ministry of the
Interior ensure a very high level of reliability and trust in the German eID. In
fact, there has not been any meaningful security incident to the eID card since
its introduction in 2010.

## Batch Credential Issuance

When the same Issuer-signed credential (as in PID option C) is reused for the
same Relying Party or across multiple Relying Parties, it becomes possible to
link these transactions and track users across different use cases. This is due
to the properties of mdoc and SD-JWT credential formats using:

- unique hashes for the disclosures
- unique public keys for key-binding
- unique PID Provider signatures
- unique reference for the status

To achieve the unlinkability requirements (as described in
[Unlinkability](../../04-privacy-and-security.md#unlinkability)), the Provider
issues a batch of credential instances that all contain the same credential
dataset but with different cryptographic material. This functionality is enabled
by using the Credential Endpoint of the OpenID4VCI protocol. It requires the
Wallet to send a number of different keys used for cryptographic binding in the
Credential Request and requires the Provider to choose individual salts for the
Disclosures (therefore resulting in individual signatures as well) in the
Credential Response. If the Wallet utilizes the batch-issued credentials as
one-time use, unlinkability for the same Relying Party and colluding Relying
Parties can be achieved. If all issued credentials have been used, the Wallet
should request new credential instances from the Provider, preferably using
Refresh Tokens to avoid a repeated user authentication.

The optimal batch size depends on several factors:

- the frequency with which the user needs to present a unique credential over
  time,
- the user experience during the issuance process when a bigger batch size is
  chosen,
- the wallet's capacity to manage a larger number of credentials, which may be
  influenced by factors such as hardware-based key storage,
- the available resources that the provider has for issuing credentials in
  larger quantities at a specific time.

The Provider is able to define a limit for the maximum number of batch size to
issue, but it is up to the wallet to choose the actual size of the batch. The
Wallet could take into account information of the individual user's credential
usage in the past.

The Provider needs to pay attention to further data in the batch-issued
credentials that may enable linkability, in particular validity dates and status
or revocation information.

Besides these efforts, users may be (intentionally) linked by the data that they
disclose to the Relying Parties. Most German citizen are trackable by the
combination of first name, last name and birth data and for certain use cases
that require to identify the user, linkability is intentional. However, the
underlying technology should aim to enable unlinkability, thus batch-issued
credentials are recommended.

## Revocation

Revocation of credentials is a process in which the issuer of PID or (Q)EAA
declares that the credential is no longer valid. Various use cases and scenarios
may require revocation:

- the PID/(Q)EAA Provider wants to revoke its issued credential because the
  contained data is no longer valid
- the Wallet Provider wants to revoke a Wallet Instance because Wallet Security
  Cryptographic Device (WSCD) or the Wallet Instance application is compromised
  or vulnerable
- the user wants to revoke their Wallet Instance because they lost their
  smartphone
- the user wants to revoke their PID
- the PID Provider wants to revoke a PID because the person has died

Regarding the various PID options, only the issuer-signed variations like C or
C' require revocation as the Authenticated Channel variations generate fresh
credentials for every transaction.

To enable revocation within the EUDI Wallet ecosystem, the following mechanisms
for revocation are considered:

- Certificate Revocation Lists
- Status Lists
- Online Certificate Status Protocol (OCSP)
- OCSP stapling

### Certificate Revocation Lists

Certificate Revocation Lists (CRL) as defined in
[RFC5280](https://datatracker.ietf.org/doc/rfc5280/) are an established
revocation mechanism for X509 certificate that has been proven in productive
environments for a long time. The Issuer maintains a publicly available list
that contains the serial numbers of revoked certificates. Relying Parties may
discover this list (e.g. as the URL is contained in the certificate) and
validate whether the serial number exists in the list.

As the issuer of a certificate always delivers the complete list, he will not
learn which serial number the Relying Party is interested in, thus guaranteeing
the unobservability. As CRLs are currently only used for X509 certificates, it
remains open to define how this mechanism is used for the PID credential
formats. Additionally, CRLs have seen some scalability limitations in the
Browser TLS context, and it remains open to evaluate if CRL sizes remain
manageable within the eIDAS ecosystem. Usage of CRL Delta updates may be
considered to increase the scalability.

### Status List

[IETF Token Status
List](https://datatracker.ietf.org/doc/draft-ietf-oauth-status-list/) is a new
mechanism with similar properties to CRLs, which defines a status mechanism for
JOSE and COSE secured Referenced Tokens. This status links to a Status List in
JSON and CBOR formats that describe the individual statuses of multiple
Referenced Tokens. The statuses of all Referenced Tokens are conveyed via a bit
array in the Status List. Each Referenced Token is allocated an index during
issuance that represents its position within this bit array. The value of the
bit(s) at this index correspond to the Referenced Token's status. The Status
List is provided by the issuer through a public endpoint or as a signed Status
List Token in JWT or CWT.

As the issuer of a status list always delivers the complete list, he will not
learn which index the Relying Party is interested in, thus guaranteeing the
unobservability. Status List have shown to have better scalability properties
than CRLs under most circumstances, but it remains open to evaluate if this is
sufficient for the eIDAS ecosystem. Status Lists have not been widely adopted
and proven in productive ecosystems yet. The Status List requires additional
communication between the issuance and revocation service during the issuance.

### Online Certificate Status Protocol (OCSP)

In the Online Certificate Status Protocol the issuer provides a service(called
OSCP Responder), which the Relying Party may use to query the revocation status
of a certificate in real time.

As the Relying Party provides the specific certificate serial number to the
issuer, unobservability can not be achieved. As OCSP is currently only used for
X509 certificates, it remains open to define how this mechanism is used for the
PID credential formats. Furthermore, the system has shown to have bad
scalability properties. Therefore, usage of OSCP is *not* recommended.

### OCSP stapling

In the OCSP stapling the TLS certificate owner regularly queries the issuer's
OCSP endpoint and "staples" the result to the response of the TLS handshake,
thus the other party does not need to fetch it themselves, reducing the traffic
and cost for the OCSP Responder.

OCSP stapling was introduced to tackle the scalability issues of OCSP but only
with modest success. OCSP stapling is currently only defined for X509
certificates, but a [proposal applying the concepts to the PID credential
formats is under
development](https://datatracker.ietf.org/doc/draft-demarco-oauth-status-attestations/).
The concept has the privacy potential as the Relying Party does not fetch the
revocation information itself, but it remains open whether sufficient
scalability can be achieved.

### Batch issuance

When using batch issuance to achieve verifier unlinkability, it is important for
privacy reasons that the status management is using unique identifiers for each
credential in the batch:

- status list: each credential needs a unique index in the status list.
- CRL: each credential needs a unique serial number.

When updating the status list and the CRL object that are public available,the
issuer should group the revocation of multiple batches into one update to avoid
linkability of the individual credentials.

### Credential Format mapping

For the PID and (Q)EAA credentials, the following mapping to the revocation
mechanisms is proposed:

- SD-JWT VC: Token Status List
- mdoc: CRL

### Online-Offline Scenarios

When the Relying Party is online, it will fetch the CRL and the Status List from
the issuer's endpoint (or from a status provider).

When the Relying Party is offline, it will use a cached version of the CRL or
the Status List with the risk of being outdated. A mechanism to pass the CRL or
the Status List to the Relying Party via the OID4VP response with a trust chain
and where the Relying Party only needs the root of trust is not defined yet to
support fully offline scenarios.

## Cross-Device PID Presentation

The PID Design Options presented in this Architecture Proposal cover the
transmission of the PID on the same device only, meaning the RP and the Wallet
shall reside on the same device.

PID scenarios across devices are not directly supported as they cannot be
secured to a Level of Assurance High. Applications wishing to support
cross-device presentations may implement a session-transfer based solution
instead: The application needs to transfer the user's session to the device
where the Wallet is installed (e.g., by displaying a QR code that is then
scanned on the second device), conduct a same-device PID presentation, and
transfer the result back via the application's session backend. Since there is
usually no authenticated channel for cross-device session transfer, fundamental
security issues may arise, for example, when QR codes are replayed in phishing
attacks. The exact implementation of cross-device session transfer flows, the
security considerations, and appropriate mitigations are out of scope of this
document.

## Repudiation

Data deniability in identification scenarios can be technically achieved through
several conceivable approaches and cryptographic primitives. A prominent example
is the German eID solution, where an ephemeral but authenticated end-to-end
channel between eID card and Relying Party is established, which is then used to
transmit the PID in an unauthenticated way. The Relying Party is able to gain
certainty about the card user's PID through the authenticity of the channel, but
is not able to prove that to a third party after the channel has been closed.
Thus, the user is able to plausibly deny the authenticity of his PID to some
third party.

Note: Technically, a Relying Party trusts the PID issuer and thus any eID card
with attested key material on its chip, which is used to compute a DH shared
secret for the establishment of an E2E channel. Therefore, it can also consider
the transmitted PID to be correct and authentic, but does not keep a proof for
this.

For establishing user deniability, PID may be protected by a MAC which was
generated with a symmetric secret key. In this case, PID authenticity can only
be verified by entities who share the secret key---usually just the two entities
involved in an identification. A third party not in possession of this key
cannot distinguish the MAC from randomness, giving the user deniability about
their presentation of the PID credential.

The use of digital signatures (for both, identification or PID) ensures PID
integrity and authenticity, but generally obstructs any kind of deniability due
to their “non-repudiation” property. A PID fragment of an identification
transaction that has been equipped with a digital signature can be regarded as
an officially certified copy, which allows to be cryptographically verifiable
verified by everyone in possession of the respective issuer’s public key. This
holds for an indefinite amount of time after the identification process. A quite
unconventional approach would be to regularly publish the respective signature
keys (including appropriate key management), but is at most conceivable for
short-lived credentials.

## PID Provider Implementation Considerations

PID Providers may be part of the Wallet solution or there can be a central PID
Provider (then also called PID Issuer) used by all German EUDI Wallets to obtain
PIDs.

If the PID Provider is part of the Wallet solution, the way PID Provider and
Wallet establish trust and communicate with each other is at the discretion of
the Wallet Provider.

If there is a PID Issuer, the communication protocol used for PID issuance and
the way trust is established needs to be standardized.

The PID Provider is a backend service that translates from German national eID
scheme into eIDAS 2.0 scheme by either issuing a PID in mdoc format or an SD-JWT
VC format in accordance to the selected transport protocol while keeping eIDAS
LoA High. The Wallet receives the data from the PID Provider and responds to the
Relying Party.

One main component of the PID Provider is an eID-Server according to BSI
TR-03130 that manages an authorization certificate with authorization bits set
according to PID specification of eIDAS 2.0. The eID-Client reads the identity
data from the German eID Card or Smart-eID, sends it to the eID-Server that
validates the data.

The second main component of the PID Provider (for Options B and C) is a service
that creates the mdoc or SD-JWT format. This includes the creation of a digital
signature or MAC over the identity data read by the eID-Server. The signature or
MAC is recognized as the PID attestation under eIDAS by Relying Parties and must
fulfill eIDAS level high. Hence, the respective key material and access to the
key material must be stored and managed in a way to resist attackers with high
attack potential.

## PID Provider Trust Management

Relying Parties must be able to establish trust in a PID. Hence, the respective
PKI must be made available to the Relying Parties, for example, through Trusted
lists (specifically for PID Providers).

## PID Contents

PIDs can be issued in two formats, mdoc and [SD-JWT
VC](https://datatracker.ietf.org/doc/draft-ietf-oauth-sd-jwt-vc/).

For SD-JWT VC, the [SD-JWT VC DM
proposal](https://github.com/danielfett/sd-jwt-vc-dm) is to be used. A type
hierarchy compliant with the SD-JWT VC DM proposal needs to be defined
consisting of a base type for EU PIDs and derived types for national PIDs. The
base type would define a minimal dataset (e.g., given name, family name, date of
birth) plus define data fields that are common to a number of national PID types
(e.g., address, nationalities, gender, middle name, birth family name, etc.).

The derived types would define the specific data fields for each national PID
type; in particular, national types would define which data fields out of those
in the base type are generally available and add specific national data fields.

For German national PIDs, the data can be obtained from an ID card, a residence
permit, or an eID card for EU citizens. The data available is defined in BSI
TR-03127. Under the assumption that data fields in the base type would be
structured and defined similar to those in [OpenID Connect
Core](https://openid.net/specs/openid-connect-core-1_0.html) and [OpenID eKYC
Identity
Assurance](https://openid.net/specs/openid-connect-4-identity-assurance-1_0.html),
a German national PID could be designed as follows:

```yaml
{
    ## Base data (SD-JWT VC DM)
    "vct": "https://example.bmi.bund.de/credential/pid/1.0",  # metadata would define this as an extension of the base type, e.g., https://example.eudi.eu/credential/pid/1.0
    "vct#integrity": "sha256-jo8433ot48....utul8ura33",

    ## Base dataset that always needs to be present

    "given_name": "Erika",             # from base type, minimal dataset, may be empty
    "family_name": "Mustermann",       # from base type, minimal dataset, may be empty
    "birthdate": "1963-08-12",         # from base type, minimal dataset, may be partial date

    ## Additional data

    "source_document_type": "id_card",  # not in base type, can also be "residence_permit" or "eu_citizen_eid_card"

    "birth_family_name": "Gabler",      # from base type

    "place_of_birth": {                 # from base type
        "locality": "Berlin",
        "country": "DE"
    },

    "also_known_as": "Schwester Agnes", # from base type

    ## data available in certain situations

    "address": {                        # from base type, available for ID card and eID card for EU citizens only
        "street_address": "Heidestraße 17",
        "locality": "Köln",
        "postal_code": "51147",
        "country": "DE"
    },

    "nationalities": [                  # from base type, not available for eID cards issued before Nov. 2019
        "DE"
    ],

    "gender": "female",                 # available for residence permit only

    ## Derived claims
    "age_equal_or_over": {              # from base type
        "12": true,
        "14": true,
        "16": true,
        "18": true,
        "21": true,
        "65": false
    },

    # key binding (SD-JWT VC DM)
    "cnf": {
        "jwk": {
            "kty": "EC",
            "crv": "P-256",
            "x": "52aDI_ur05n1f_p3jiYGUU82oKZr3m4LsAErM536crQ",
            "y": "ckhZ-KQ5aXNL91R8Eufg1aOf8Z5pZJnIvuCzNGfdnzo"
        }
    },

    ## Mandatory metadata as defined by PID Rule Book v1.0.0 section 2.4

    "iat": 1712231700,
    "exp": 1806839700

}
```

All claims except `vct`, `vct#integrity`, `cnf` and the claims listed under
metadata would be issued as selectively disclosable claims.

The additional claims `iat` and `exp` would define the lifetime of the PID. In
Options B, B' and D, this would be a short time frame, while in Options C and
C', the PID could be valid for a longer time frame.

Alternatively, multiple types could be used to distinguish between the different
source document types. This would allow for a more fine-grained specification of
available data fields. For example, a type hierarchy similar to the following
could be created:

- Base Type: `https://example.eudi.eu/credential/pid/1.0`
    - German Base Type: `https://example.bmi.bund.de/credential/pid/base/1.0`
        - PID based on ID Card:
       `https://example.bmi.bund.de/credential/pid/id-card/1.0`
        - PID based on Residence Permit:
       `https://example.bmi.bund.de/credential/pid/residence-permit/1.0`
        - PID based on EU Citizen eID card:
       `https://example.bmi.bund.de/credential/pid/eu-citizen-eid-card/1.0`
    - (... other national PID types)

While this design option provides more granular credential types, it has a
privacy drawback: The source of the PID information is always revealed in the
credential type URI and would not be selectively disclosable. If the credential
is used, for example, for age verification, the Verifier learns not only the
age, but also that the person presenting the credential is not a German citizen.
Therefore, a single type for all national PIDs is preferred.

PIDs are issued as compact-serialized SD-JWTs.

For the mdoc format, the PID contents as defined in the [ARF PID rule
book](https://eu-digital-identity-wallet.github.io/eudi-doc-architecture-and-reference-framework/1.2.0/annexes/annex-06-pid-rulebook.pdf)
are to be used.

<!-- PDF-show
\newpage
-->

## PID SD-JWT VC Type Metadata

This section illustrates a first draft of the SD-JWT VC Type Metadata for the
german PID. Note that the values of the fields `vct`, `extends` and
`extends#integrity` are only placeholders and will be changed before this
definition reaches a final stage.

```json
{
  "vct": "https://example.bmi.bund.de/credential/pid/1.0",
  "name": "German Person Identification Data Credential - First Version",
  "description": "The definition of the core identification credential for all natural persons in Germany - first revision",
  "extends": "https://example.eudi.eu/credential/pid/1.0",
  "extends#integrity": "sha256-9cLlJNXN-TsMk-PmKjZ5t0WRL5ca_xGgX3c1VLmXfh-WRL5",
  "display": [
    {
      "lang": "en-US",
      "name": "German Person Identification Data Credential",
      "description": "The core identification credential for all natural persons in Germany"
    },
    {
      "lang": "de-DE",
      "name": "Deutscher Personenidentifikationsnachweis",
      "description": "Der zentrale Identifikationsnachweis für alle natürlichen Personen in Deutschland"
    }
  ],
  "claims": [
    {
      "path": [
        "vct"
      ],
      "sd": "never"
    },
    {
      "path": [
        "vct#integrity"
      ],
      "sd": "never"
    },
    {
      "path": [
        "given_name"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Vorname"
        },
        {
          "lang": "en-US",
          "label": "Given Name"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "family_name"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Nachname"
        },
        {
          "lang": "en-US",
          "label": "Last Name"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "birthdate"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Geburtsdatum"
        },
        {
          "lang": "en-US",
          "label": "Birthdate"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "source_document_type"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Quelldokumenttyp",
          "description": "Der Typ der Quelle des Dokumentes, beispielweise der Personalausweis oder der Aufenthaltstitel"
        },
        {
          "lang": "en-US",
          "label": "Source Document Type",
          "description": "The type of the source of the document, for example the national identity card or the residence title"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "address"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Adresse"
        },
        {
          "lang": "en-US",
          "label": "Address"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "address",
        "street_address"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Straße"
        },
        {
          "lang": "en-US",
          "label": "Street Address"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "address",
        "locality"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Ort"
        },
        {
          "lang": "en-US",
          "label": "Locality"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "address",
        "postal_code"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Postleitzahl"
        },
        {
          "lang": "en-US",
          "label": "Postal Code"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "address",
        "country"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Land"
        },
        {
          "lang": "en-US",
          "label": "Country"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "nationalities"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Staatsangehörigkeiten"
        },
        {
          "lang": "en-US",
          "label": "Nationalities"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "gender"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Geschlecht"
        },
        {
          "lang": "en-US",
          "label": "Gender"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "birth_family_name"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Geburtsname"
        },
        {
          "lang": "en-US",
          "label": "Family Name at Birth"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "place_of_birth"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Geburtsort"
        },
        {
          "lang": "en-US",
          "label": "Place of Birth"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "place_of_birth",
        "locality"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Geburtsort"
        },
        {
          "lang": "en-US",
          "label": "Place of Birth"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "place_of_birth",
        "locality"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Ort"
        },
        {
          "lang": "en-US",
          "label": "Locality"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "place_of_birth",
        "country"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Land"
        },
        {
          "lang": "en-US",
          "label": "Country"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "also_known_as"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Ordens- oder Künstlername"
        },
        {
          "lang": "en-US",
          "label": "Religious Name or Pseudonym"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "age_equal_or_over"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Altersbestätigung"
        },
        {
          "lang": "en-US",
          "label": "Age Verification"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "age_equal_or_over",
        "12"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Mindestalter 12"
        },
        {
          "lang": "en-US",
          "label": "Minimum Age 12"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "age_equal_or_over",
        "14"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Mindestalter 14"
        },
        {
          "lang": "en-US",
          "label": "Minimum Age 14"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "age_equal_or_over",
        "16"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Mindestalter 16"
        },
        {
          "lang": "en-US",
          "label": "Minimum Age 16"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "age_equal_or_over",
        "18"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Mindestalter 18"
        },
        {
          "lang": "en-US",
          "label": "Minimum Age 18"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "age_equal_or_over",
        "21"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Mindestalter 21"
        },
        {
          "lang": "en-US",
          "label": "Minimum Age 21"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "age_equal_or_over",
        "65"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Mindestalter 65"
        },
        {
          "lang": "en-US",
          "label": "Minimum Age 65"
        }
      ],
      "sd": "always"
    },
    {
      "path": [
        "cnf"
      ],
      "sd": "never"
    },
    {
      "path": [
        "iat"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Ausstellungsdatum"
        },
        {
          "lang": "en-US",
          "label": "Issuing Date"
        }
      ],
      "sd": "never"
    },
    {
      "path": [
        "exp"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Ablaufdatum"
        },
        {
          "lang": "en-US",
          "label": "Expiry Date"
        }
      ],
      "sd": "never"
    },
    {
      "path": [
        "issuing_authority"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Ausstellende Behörde"
        },
        {
          "lang": "en-US",
          "label": "Issuing Authority"
        }
      ],
      "sd": "never"
    },
    {
      "path": [
        "issuing_country"
      ],
      "display": [
        {
          "lang": "de-DE",
          "label": "Ausstellungsland"
        },
        {
          "lang": "en-US",
          "label": "Issuing Country"
        }
      ],
      "sd": "never"
    }
  ],
  "schema": {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "German Person Identification Data VCT Schema",
    "description": "The JSON schema that defines the German Person Identification Data VCT",
    "type": "object",
    "properties": {
      "vct": {
        "type": "string"
      },
      "vct#integrity": {
        "type": "string"
      },
      "given_name": {
        "type": "string"
      },
      "family_name": {
        "type": "string"
      },
      "birthdate": {
        "type": "string",
        "format": "date"
      },
      "source_document_type": {
        "type": "string"
      },
      "address": {
        "type": "object",
        "properties": {
          "street_address": {
            "type": "string"
          },
          "locality": {
            "type": "string"
          },
          "postal_code": {
            "type": "string"
          },
          "country": {
            "type": "string"
          }
        },
        "minProperties": 1
      },
      "nationalities": {
        "type": "array",
        "items": {
          "type": "string"
        },
        "minItems": 1,
        "uniqueItems": true
      },
      "gender": {
        "type": "string"
      },
      "birth_family_name": {
        "type": "string"
      },
      "place_of_birth": {
        "type": "object",
        "properties": {
          "locality": {
            "type": "string"
          },
          "country": {
            "type": "string"
          }
        },
        "minProperties": 1
      },
      "also_known_as": {
        "type": "string"
      },
      "age_equal_or_over": {
        "type": "object",
        "properties": {
          "12": {
            "type": "boolean"
          },
          "14": {
            "type": "boolean"
          },
          "16": {
            "type": "boolean"
          },
          "18": {
            "type": "boolean"
          },
          "21": {
            "type": "boolean"
          },
          "65": {
            "type": "boolean"
          }
        },
        "minProperties": 1
      },
      "cnf": {
        "type": "object"
      },
      "iat": {
        "type": "number"
      },
      "exp": {
        "type": "number"
      },
      "issuing_authority": {
        "type": "string"
      },
      "issuing_country": {
        "type": "string"
      }
    },
    "required": [
      "vct",
      "source_document_type",
      "cnf",
      "iat",
      "exp",
      "issuing_authority",
      "issuing_country"
    ]
  }
}
```
