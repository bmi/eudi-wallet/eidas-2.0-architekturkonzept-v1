<!-- PDF-show
\newpage
-->

# Wallet Function: (Q)EAA Issuance and Presentation

The updated eIDAS regulation and the Architecture Reference Framework (ARF)
describe Electronic Attestation of Attributes (EAA) as generic, general-purpose
electronic credentials that attest to any user data. QEAAs are provided by
Qualified Trust Service Providers (QTSP) governed under eIDAS. EAAs are provided
by any Trust Service Provider that, while supervised under eIDAS, may rely on
other contractual frameworks and trust domains. EAAs may also be provided by or
on behalf of a public sector body responsible for an authentic source (called
Pub-EAA). In this case, Pub-EAAs must match specific requirements equally to
QEAAs, and both shall have the same legal effect as lawfully issued attestations
in paper form. Any Relying Party may request presentations for QEAAs and EAAs if
they are authorized under the eIDAS trust management for RPs.

The interaction between QTSPs for QEAA and authentic sources involves several
steps to ensure data integrity and security. Data from authentic sources must be
transmitted to the QTSP in a secure format.

To issue a QEAA into the wallet, the following security measures are required:

- **Data Confidentiality**: All data transmitted from the authentic source to
  the QTSP must be encrypted to prevent unauthorized access.
- **Data Integrity**: The data must be digitally signed by the authentic source
  to verify its authenticity and integrity.
- **Compliance with eIDAS Section 3 and Art. 24**: The QTSP must ensure that all
  processes comply with the requirements outlined in Section 3 of eIDAS and
  specifically Art. 24, which details the requirements for qualified trust
  service providers.
- **Interoperability**: The QTSP must adhere to the ongoing ETSI standardization
  efforts to ensure interoperability and compliance with European standards.

These measures ensure that the QEAA issued into the wallet is secure, authentic,
and compliant with the relevant regulations and standards.

## Use Case Examples

The following use cases are examples for (Q)EAAs:

- mobile driving license
- municipal ID
- educational credentials, e.g. diploma
- payment
- ePrescription
- public transport ticket

## Requirements

- Technical Interoperability with standards referenced by implementing acts acc
  Art. 45 c, d and eIDAS
- Remote issuance
- Presentation for remote and proximity flows
- Support level of assurance acc. Art. 24 eIDAS required by use cases (details
  TBD)

## Credential Formats & Protocols

As the PID and a (Q)EAA have the same technical foundation, the credential
formats and transport protocols of the PID should also apply to (Q)EAAs. To
achieve the best interoperability, the EUDIW should be limited to as few
technological options as possible.

For the transport protocols the EUDIW shall support:

- OpenID4VCI
  - in Pre-Authorized Code Flow
  - in Authorization Code Flow
- OpenID4VP

For credential formats the EUDIW shall support:

- ISO mdoc according to 18013-5
- IETF [SD-JWT VC](https://datatracker.ietf.org/doc/draft-ietf-oauth-sd-jwt-vc/)

Other credential formats can be used but the description will only focus on the two mentioned ones.

## Remote Issuance Flow

[Description](../../flows/EAA-Issuance-OpenID4VC.md)

### User Experience for remote issuance

[User Journey: (Q)EAA Issuance - Authorization
Code](../../user_journeys/QEAA-AuthorizationCode-issuance.png)

[User Journey: (Q)EAA Issuance - Pre-Authorized
Code](../../user_journeys/QEAA-PreauthorizedCode-issuance.png)

*Via the link, the user journey of the sequence diagram can be viewed in the
form of lo-fi wireframe screen sequences. All screens of the User Journey are
labeled with a specific name. Within the associated sequence diagram, green
hexagonal markers labeled "Screen: screen_name" are placed to indicate the
corresponding screen in that sequence. The Lo-fi wireframes primarily serve to
illustrate the architecture flow from the UX/UI perspective and to show how an
implementation could look from the UX/UI perspective. However, it should be kept
in mind that the actual realization and implementation of the UX/UI is the
responsibility of the wallet providers and the relying parties.*

## Remote Presentation Flow

[Description](../../flows/EAA-Presentation-OpenID4VC.md)

### User Experience for remote presentation

[User Journey: (Q)EAA Presentation - Same
Device](../../user_journeys/QEAA-SameDevice-presentation.png)

[User Journey: (Q)EAA Presentation - Cross
Device](../../user_journeys/QEAA-CrossDevice-presentation.png)

*Via the link, the user journey of the sequence diagram can be viewed in the
form of lo-fi wireframe screen sequences. All screens of the User Journey are
labeled with a specific name. Within the associated sequence diagram, green
hexagonal markers labeled "Screen: screen_name" are placed to indicate the
corresponding screen in that sequence. The Lo-fi wireframes primarily serve to
illustrate the architecture flow from the UX/UI perspective and to show how an
implementation could look from the UX/UI perspective. However, it should be kept
in mind that the actual realization and implementation of the UX/UI is the
responsibility of the wallet providers and the relying parties.*

## Proximity Presentation Flow

*NOTE: THIS SECTION WILL BE DEVELOPED IN A FUTURE RELEASE*

## (Q)EAA Provider Trust Management

*NOTE: THIS SECTION WILL BE DEVELOPED IN A FUTURE RELEASE*
