  // Wide diagrams receive enormously big height. This code
// assigns a special class .wide-svg to each svg where width > height
// and CSS fixes the problem.
document.addEventListener("DOMContentLoaded", function() {
    const svgs = document.querySelectorAll('.puml .diagram');
    svgs.forEach(svg => {
        console.log(svg);
        // Get the computed width and height of each SVG
        // const rect = svg.querySelector('rect');
        let width = svg.getAttribute('width');
        let height = svg.getAttribute('height');
  
        width = parseInt(width);
        height = parseInt(height);
        
        if(isNaN(width) || isNaN(height)) {
            return
        }      
        svg.classList.add('wide-svg');
    });
/*     setTimeout(() => {
    const graphElement = document.getElementById("graph0");

    if (graphElement) {
        // Get the current 'transform' attribute value
        const transformAttr = graphElement.getAttribute("transform");

        if (transformAttr) {
            console.log("Original transform attribute:", transformAttr);

            // Apply it as an inline style
            graphElement.style.transform = formatTranslate(transformAttr);

            console.log("Applied transform as inline style:", graphElement.style.transform);
        } else {
            console.warn("No transform attribute found on #graph0");
        }
    } else {
        console.error("Element with ID 'graph0' not found.");
    }
    }, 100); */
});

function formatTranslate(transform) {
    const translateRegex = /translate\(([^)]+)\)/;
    const match = transform.match(translateRegex);
    if (match) {
        const values = match[1].split(' ');
        return `scale(1) translate(${values[0]}px, ${values[1]}px)`;
    }
    return null;
}