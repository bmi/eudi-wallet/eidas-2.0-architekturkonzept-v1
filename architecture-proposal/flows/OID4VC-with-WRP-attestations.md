# OID4VC with Wallet-Relying Party Attestations

When dealing with [Wallet-Relying Party
Authentication](./Wallet-Relying-Party-Authentication.md) or [Disclosure
Policies](../functions/06-disclosure-policies(index.md), the issued certificates
like access, registration, and authorization certificates should be included in
the request from the Wallet-Relying Party. When they need to be fetched by the
Wallet from the RP registrar, the Registrar would be able to monitor which
wallet is interacting with which Relying Party. Instead the Wallet will only
request the status information from the registrar by fetching the CRL or Status
List.

## Issuance

### OID4VCI flow with access certificate

Before issuing a credential into the Wallet, the Issuer has to authenticate
against the EUDI Wallet before it receives the credential or presents any
information during the authorization phase. To do this, the issuer has to sign
its published [Credential Issuer
Metadata](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html#name-credential-issuer-metadata).

```plantuml

@startuml
'Ensure messages are not too wide
skinparam maxMessageSize 200
skinparam wrapWidth 300


'Macro for colored [TLS] block
!function tls()
!return "<color #118888>[TLS]</color>"
!endfunction

'Align text on arrows to center
skinparam sequenceMessageAlign center

'padding between boxes
skinparam BoxPadding 100

autonumber "<b>(000)"

title OpenID4VCI with Provider Authentication

participant w as "User's EUDI Wallet Instance"
participant i  as "(Q)EAA Provider"            
participant rpr as "Wallet-Relying Party Registrar"
participant eu as "EU List of RP Registrars"

alt Fetch Trust Anchor
  w -> eu: tls() HTTP GET ./eu-registrar-list
  eu -> w: tls() HTTP 200 <EU Registrar List>
end
w -> i : tls() HTTP GET ./well-known Credential Issuer Metadata
i -> w : tls() HTTP 200 <Credential Issuer Metadata>
w -> w : validates RP identity
w -> rpr: tls() HTTP GET ./status CRL
rpr -> w: tls() HTTP 200 <CRL>

@enduml
```

Step by Step:

1. Wallet fetches the trust anchor from the EU
    - List of List as the Root of Trust
    - This step must not be done for each request, but the list should be up to
      date
2. Wallet downloads and stores the list of RP Registrars
3. Wallet fetches the Credential Issuer Metadata from the Issuer
4. Issuer response with the Credential Issuer Metadata
    - If no signed metadata is provided, the process stops
    - The [Access
      Certificate](./Wallet-Relying-Party-Authentication.md#access-certificate)
      is included in the header of the JSON Web Signature
5. Wallet is validating the RP's identity
    - Wallet validates the chain of trust of the request form the [Access
      Certificate](./Wallet-Relying-Party-Authentication.md#access-certificate)
      to the list of list it fetched before
    - In case of a the chain of trust is not valid, the process stops
6. Wallet fetches the CRL from the RP Registrar
    - In case the trust chain is revoked, the process stops
7. RP Registrar response with the CRL
    - If no CRL is provided or one certificate is revoked, the process stops

## Presentation

### OID4VP flow with certificates

The following diagram outlines the process of a presentation request where the
Relying-Party with the required certificates.

```plantuml
@startuml

'Ensure messages are not too wide
skinparam maxMessageSize 200
skinparam wrapWidth 300

'Align text on arrows to center
skinparam sequenceMessageAlign center

'padding between boxes
skinparam BoxPadding 100

autonumber "<b>(000)"

title Presentation Request

participant u order 1 [
    User
]

participant w order 2 [
    Wallet
]

participant rp order 3 [
    Relying Party
]

participant rpr order 4 [
    Wallet-Relying Party Registrar
]

participant i order 5 [
    (Q)EAA Provider  
]

participant eu order 6 [
    EU List of RP Registrars
]

eu <-> w: fetches trust anchor
rp -> rp: creates presentation request
rp -> w: sends presentation request
note right: certificates are attached
w -> w: validates RP identity
note right: trustchain is x509 based (PKI)
w -> w: validates intended use
note right: validates trustchain 
alt Validate policy
  w <-> i: fetches issuer policy
  w -> w: validates policy
end
w -> u: request suser interaction
u -> w: approves
w -> rp: sends presentation response
@enduml
```

Step by Step:

1. Wallet fetches the trust anchor from the EU
    - List of List as the Root of Trust
    - This step must not be done for each request, but the list should be up to
      date
2. RP creates the [presentation request](#presentation-request)
    - Uses the key from the access certificate to sign it
    - Includes all [Registration
      Certificates](./Wallet-Relying-Party-Authentication.md#registration-certificate)
      that cover the values that should be requested
    - Includes the [Authorization
      Certificates](../functions/06-disclosure-policies/index.md#authorization-certificate)
      that are required for the request.
3. RP sends [presentation request](#presentation-request) to Wallet
4. Wallet is validating the RP's identity
    - Wallet validates the chain of trust of the request form the [Access
      Certificate](./Wallet-Relying-Party-Authentication.md#access-certificate)
      to the list of list it fetched before
    - In case of a the chain of trust is not valid, the process stops
5. Wallet validates requested data against intended use
    - Validates the chain of trust of all passed [Registration
      Certificates](./Wallet-Relying-Party-Authentication.md#registration-certificate)
    - Validates if the requested data is equal or a subset of the Registration
      Certificates
    - If one of the validations fails, the process is aborted. The user is able
      to report the incident, where it sends all the information to service to
      validate it. The report process is out of scope of this spec.
6. Optional: Wallet fetches the issuer authorization policy from the issuer
   metadata
    - In case none is defined, step 7 is skipped
7. Optional: Wallet validates the policy against the the passed [Authorization
   Certificates](../functions/06-disclosure-policies/index.md#authorization-certificate)
    - Validates chain of trust
    - Validates if all requested credentials are covered with the policy
    - In case one of the validation is invalid, show a warning to the user, but
      do not abort the flow (RP could still have a valid reason to fetch it).
8. Wallet requests a user interaction
    - User gets the information who is requesting data with which purpose
    - In case a warning from step 7 was generated, show it here
9. User approves the request
10. Wallet creates a presentation response and sends it to the Relying Party

The flow continues by the verification of the response by the RP.

When fetching the policy for step 6, there is a minimal risk for the Wallet to
be monitored by the Issuer:

### Presentation request

The credentials set or its sub elements must not include a `purpose` value since
it is overruled by the one defined in the authorization certificate.

The request includes a new `credentials` attribute, that allows to attach
certificates like registration and authorization certificates.

- **`type`**: STRING, media type of the attached credential, helping to parse
  the credential by the Wallet. The value has to be registered in the [IANA
  media types](https://www.iana.org/assignments/media-types/media-types.xhtml)
  registry.
- **`data`**: ANY, content of the credential. Will be parsed by the Wallet based
  on the type.
- **`credentialsMatch`**: STRING[], when the provided certificate is used for
  authorization, it will list the credential ids from the `dcql_query`.

In case the registration or authorization certificate the media types could be
[jose](https://www.iana.org/assignments/media-types/application/jose) or
[jose-json](https://www.iana.org/assignments/media-types/application/jose+json)
depending on the serialization.

```json
{
  "x5c": [], // chain of trust, starts with access certificate
  "typ": "JWT",
  "alg": "ES256"
}
.
{
  "response_type":"vp_token",
  "client_id":"x509_san_dns:client.example.org",
  "response_mode":"direct_post",
  "dcql_query":{
    "credentials":[
      {
        "id":"my_credential",
        "format":"vc+sd-jwt",
        "meta":{
          "vct_values":[
            "https://credentials.example.com/identity_credential"
          ]
        },
        "claims":[
          {
            "path":[
              "last_name"
            ]
          },
          {
            "path":[
              "first_name"
            ]
          },
          {
            "path":[
              "address",
              "street_address"
            ]
          }
        ]
      }
    ]
  },
  "credentials":[
    {
      "type:" "jose",
      "data": "base64-encoded-registrationCertificate-jws"
    },
    {
      "type:" "jose",
      "data": "base64-encoded-authorizationCertificate-jws",
      "credentialsMatch": ["my_credential"]
    }
  ]
}
```

### Open Topics

#### Support of Proximity Flow

For the Proximity Flow of the MDL, the use of the Query Language defined by MDL
has been suggested. In this context, the Registration and Authorization
Certificates should also be issued as COSE_Signature, as JWS is not utilized in
MDL. It remains to be evaluated whether the two certificates can be attached
within the proximity flow or if they must be retrieved from the RP registrar.
The latter approach may introduce potential privacy risks that are intended to
be avoided.
