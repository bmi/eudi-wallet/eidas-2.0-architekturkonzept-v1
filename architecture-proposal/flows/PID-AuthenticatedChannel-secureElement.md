<!-- PDF-show
\newpage
-->

# PID Option D: Authenticated Channel with Secure Element

## Basic Ideas

* There is an Applet in the Secure Element of the user's device. This applet
  belongs to the Wallet Provider's domain (difference in case of eUICC?). It
  could be used for very security sensitive credentials beyond PIDs.
* The Wallet Design is composed of three elements: Wallet Backend, Wallet App
  and Wallet Applet
* Applet is installed during Wallet App installation (if supported by the device
  and provisioning is possible through platform vendor/MNO)
* Applet is provisioned with an assertion attesting applet managed key as
  (internally used) authentication key
* Access to authentication key is protected by Applet-managed PIN

Note: This flow is shown here using the SD-JWT credential format, but the mdoc
format can be used as well.

## Responsibilities of the Wallet Components

Wallet App

* RP authentication
* Request processing
* Combination of PID with (Q)EAA presentations
* Response encryption
* Access to Wallet App and Data managed by the App is protected by platform
  authentication

Wallet Applet

* Management of PID specific device keys
* Access to PID device keys is protected by Applet-managed PIN
* Creation of proof of possession for wallet attestations (with PID device keys)
* Data Storage for PIDs and PID data (could also just HMAC and encrypt and hand
  them over to App)
* creation of credential presentations (not responses!)

Wallet Provider

* Applet provisioning with authentication key
* Attestation of Wallet App and PID device keys

## Cryptographic Formats

### Issuance

Long-term keys:

* Authentication key of Wallet Applet: $(auth\_pub, auth\_priv)$ — authenticates
  the applet
* Wallet provider backend has keys: $(wp\_pub, wp\_priv)$
* PID Provider backend has keys: $(pp\_pub, pp\_priv)$

Transaction-specific keys:

* Wallet Secure Area generates device key $(device\_pub, device\_priv)$ *
 authenticity can be determined by applet assertion* a new key is generated
 per issuer
* Wallet Secure Area generates DPoP key $(dpop\_pub, dpop\_priv)$
* Wallet generates ephemeral key pair used to encrypt the credential response
  $(cre\_eph\_pub, cre\_eph\_priv)$

Artifacts:

* Wallet applet creates device key PoP: $device\_key\_pop :=
  \text{sign}(wallet\_attestation\_nonce)_{device\_priv}$
* Wallet applet creates DPoP proof: $dpop\_proof := \text{sign}(dpop\_nonce,
  dpop\_pub, iat)_{dpop\_priv}$
* Wallet applet creates signed assertion: $device\_key_\_attestation :=
  \text{sign}(device\_pub)_{auth\_priv}$
* Wallet app creates wallet app attestation: $wallet\_app\_attestation :=
  \text{app\_attestation}(wallet\_attestation\_nonce)_\text{os\_keys}$
* Wallet backend creates wallet attestation: $wallet\_attestation :=
  \text{sign}(device\_pub)_{wp\_priv}$
* Wallet creates wallet attestation PoP: $wallet\_attestation\_pop :=
  \text{sign}(pid\_issuer\_session\_id)_{device\_priv}$
* Wallet applet creates device key PoP for credential issuance:
  $device\_key\_pop := \text{sign}(c\_nonce)_{device\_priv}$
* PID Provider creates $x5c\_header := \text{sign}(device\_pub)_{pp\_priv}$
* PID Provider creates $user\_data\_jwt := \text{hmac}(user\_data,
  x5c\_header)_{\text{ecdh(device\_pub, pp\_pub)}}$

### Presentation

Long-term keys:

* Device key $(device\_pub, device\_priv)$

Transaction-specific keys:

* **RP contribution to HMAC key:** RP generates ephemeral key pair
  $(rp\_eph\_pub, rp\_eph\_priv)$
* **KB-JWT Keys:** Wallet applet generates ephemeral key pair $(kb\_eph\_pub,
  kb\_eph\_priv)$

Artifacts:

* $hmac\_key := \text{ecdh}(device\_priv, rp\_eph\_pub)$
* $sd\_jwt := \text{hmac}(x5c\_header, \mathit{eID\_data},
  kb\_eph\_pub)_{hmac\_key}$
* $kb\_jwt := \text{sign}(nonce, audience, \text{hash}(sd\_jwt,
  disclosures))_{kb\_eph\_priv}$

<!-- PDF-show
\newpage
\blandscape
-->

### Dependencies

*TODO: May want to expand to include metadata.*

```plantuml
digraph G {

  subgraph cluster_pid_provider {
    style=filled
    color=lightblue
    label="PID Provider"
    pp [label="🗝 (pp_priv, pp_pub)"]
  }

  pp -> device_key [label="sign (x5c header)"]

  subgraph cluster_rp {
    style=filled
    color=lightyellow
    label="Relying Party"
    rp [label="🗝 (rp_priv, rp_pub)"]
    rp_eph [label="🗝 (rp_eph_priv, rp_eph_pub)"]
    rp -> rp_eph [label="sign (Auth. Request)"]
  }

  subgraph cluster_wallet {
    style=filled
    color=lightgreen
    label="Wallet"

    subgraph cluster_app {
        label="App"
        style=filled
        color=aquamarine
    }

    subgraph cluster_applet {
      label="Applet"
      style=filled
      color=cyan

      auth_key [label="🗝 (auth_priv, auth_pub)"]
      user_data [label="eID data"]
      shared_secret_k [label="🗝 shared secret K"]
      sd_jwt_hmac_key [label="🗝 hmac key"]
      nonce_audience [label="nonce, audience"]
      kb_eph [label="🗝 (kb_eph_priv, kb_eph_pub)"]

      auth_key -> device_key [label="sign (device key attestation)"]
      device_key [label="🗝 (device_priv, device_pub)"]
      device_key -> wallet_attestation_nonce [label="sign (device key PoP)"]
      device_key -> pid_issuer_session_id [label="sign (wallet attestation PoP)"]
      device_key -> c_nonce [label="sign (credential issuance PoP)"]
      device_key -> shared_secret_k [label="ecdh-ka"]
      rp_eph -> shared_secret_k [label="ecdh-ka"]
      shared_secret_k -> sd_jwt_hmac_key [label="key derivation"]
      sd_jwt_hmac_key -> user_data  [label="hmac (SD-JWT)"]
      sd_jwt_hmac_key -> kb_eph [label="hmac (SD-JWT)"]
      kb_eph -> nonce_audience [label="sign (KB-JWT)"]

      dpop_key [label="🗝 (dpop_priv, dpop_pub)"]
      dpop [label="DPoP proof (dpop_nonce)"]
      dpop_key -> dpop [label="sign"]
    }

    subgraph cluster_backend {
        label="Backend"
        style=filled
        color=cadetblue1

        wp [label="🗝 (wp_priv, wp_pub)"]
        wp -> device_key [label="sign (wallet attestation)"]
    }

    wallet_attestation_nonce [label="wallet attestation nonce"]
  }

  subgraph cluster_os {
    style=filled
    color=lightgrey
    label="OS"
    os [label="🗝 (os_priv, os_pub)"]
  }

  os -> wallet_attestation_nonce [label="sign (wallet app attestation)"]

}
```

Not shown: `user_data_jwt`

Note: x5c header does not have to be signed in SD-JWT

<!-- PDF-show
\elandscape
\newpage
-->

## Sequence Diagrams and Step-by-Step Description of Each Step

Each step of the protocol will be explained using a sequence diagram followed by
a step-by-step description.

### Wallet Activation

[User Journey: Wallet Activation - Authenticated Channel - Secure
Element](../user_journeys/PID-AuthenticatedChannel-secureElement-initialization.png)

To initialize the wallet, the Wallet obtains wallet attestation as defined in
[Wallet Attestation](Wallet-Attestation.md).

### Credential Issuance

#### Credential Issuance Sequence Diagram

[User Journey: PID Issuance - Authenticated Channel - Secure
Element](../user_journeys/PID-AuthenticatedChannel-secureElement-issuance.png)

```plantuml
@startuml
'Ensure messages are not too wide
skinparam maxMessageSize 200
skinparam wrapWidth 500

'Macro for colored [TLS] block
!function tls()
!return "<color #118888>[TLS]</color>"
!endfunction

'Align text on arrows to center
skinparam sequenceMessageAlign center

'padding between boxes
skinparam BoxPadding 100

autonumber "<b>(000)"

title PID Issuance over OpenID4VCI using AuthenticatedChannel and Secure Element

actor u as "User\nOpenID Holder"
participant wscd [
                  Wallet Secure Cryptographic Device
                  (WSCD)
                  ----
                  Long-term Key: (//device_pub//, //device_priv//)
                 ]
participant w as "User's EUDI Wallet Instance\n(eID-Client)"
participant i  [
                PID Provider
                (eService+eID Server)
                ----
                Long-term Key: (//pp_pub//, //pp_priv//)
            ]

note over wscd, w: assumption: wallet applet already provisioned and set up with a wallet applet attestation the wallet provider can use to authenticate the applet instance, user authentication with dedicated PIN

u --> w : open wallet, unlock wallet
hnote over w #dfd: Screen: launch_wallet
hnote over w #dfd: Screen: unlock_wallet
u --> w : request issuance of PID
hnote over w #dfd: Screen: credential_catalog

note over w,i: PID Issuer and EUDI Wallet have inherent trust relationship, metadata may be pre-configured or retrieved

w -> i : tls() HTTP POST </session_endpoint> wallet attestation nonce
i -> i : generate and store nonce
i --> w : tls() HTTP 200 <wallet attestation nonce>

w <-> wscd : get proof of possession for wallet attestation (wallet attestation nonce)

note right : Attestation guarantees with high certainty that Wallet is trustworthy and not manipulated

w -> i : tls() HTTP POST PAR (client_id, code_challenge, wallet attestation+PoP, either scope or authorization_details)
i -> i : verify wallet attestation and check Wallet Provider solution status on trust list
i --> w : tls() HTTP 200 request_uri

w -> i : tls() HTTP GET <OpenID4VCI Authorization Request(request_uri)>

hnote over w #dfd: Screen: consent_add_credential
hnote over w #dfd: Screen: eid_start

group Read eID or Smart eID acc. to BSI TR-03130
i --> w : tls() HTTP 200 starting the eID Process
w <-> i : eID Process
u <--> w : <eID-PIN>
hnote over w #dfd: Screen: eid_pin

w <-> i : eID Process
w -> i : tls() HTTP GET finishing the eID process with refreshUrl
hnote over w #dfd: Screen: eid_nfc_data
end

i --> w : tls() HTTP 302 <OpenID4VCI Authorization Response(authorization code)>

group Generate initial DPoP nonce
  w -> w : generate placeholder DPoP proof with generic (not WSCD-bound) key pair

  w -> i : tls() HTTP POST <Token Request(DPoP Header with placeholder proof, authorization_code, PKCE code_verifier)>
  i -> i: generate and store dpop_nonce
  i -> w : tls() HTTP 400 <Bad Request (DPoP nonce Header with dpop_nonce, error: "use_dpop_nonce")>

  note left : The Wallet should check at this point, whether the Token Endpoint delivered the expected error and nonce. If not, this needs to be handled (retry or abort gracefully).
  w -> w: store dpop_nonce
end

w -> w: prepare DPoP proof JWT with //dpop_pub//, dpop_nonce, iat
w <-> wscd: hash and sign DPoP proof JWT with //dpop_priv//

w -> i : tls() HTTP POST <Token Request(authorization code, PKCE code_verifier, wallet attestation, DPoP Header)>
i -> i: generate and store dpop_nonce
i -> i : lookup authorization code\ngenerate TokenResponse with DPoP access token\nverify PKCE challenge\nverify DPoP proof
i --> w : tls() HTTP 200 <Token Response(DPoP Header with proof, DPoP-bound access_token, c_nonce, optional authorization_details)>

w -> w: prepare DPoP proof JWT with //dpop_pub//, dpop_nonce, iat
w <-> wscd: hash and sign DPoP proof JWT with //dpop_priv//

w <-> wscd : generate of proof of possession for attested key //device_pub// with c_nonce
hnote over w #dfd: Screen: wallet_pin

w -> w : generate credential response encryption key pair (//cre_eph_pub//, //cre_eph_priv//)
w -> w : create credential_response_encryption object with jwk containing //cre_eph_pub//

w -> i: tls() Credential Request (DPoP Header with proof, DpoP bound access token, format, credential_response_encryption object, //device_pub//)
i -> i: verify the access token\nverify DPoP proof
i -> i: perform ECDH-KA with //device_pub// and //pp_priv// and derive session keys
i -> i: create user data JWT from eID data and HMAC
i -> i : create certificate chain with //device_pub// as leaf and sign with //pp_priv//

i -> i : generate encrypted credential response JWT using the values received in the credential_response_encryption object
note over i, w: repurposing OpenID4VCI as certificate provisioning, instead of credential issuance
i --> w: tls() Credential Response JWT with user data as JWT containing signed //device_pub// in x5c header

w -> w : decrypt credential response JWT and retrieve user data JWT
w --> wscd: user data JWT with x5c header
wscd -> wscd: perform ECDH-KA with //device_priv// and //pp_pub// and derive session keys, validate HMAC of user data JWT
wscd -> wscd: securely store user data, x5c header and associated (//device_pub//,//device_priv//)
wscd --> w: return credential handle
hnote over w #dfd: Screen: success
hnote over w #dfd: Screen: home

@enduml
```

<!-- PDF-show
\newpage
\blandscape
-->

### Credential Issuance Step-by-Step Description

1. The user opens and unlocks their Wallet
2. The user browses through the pre-configured credential catalog and chooses to
   request a PID
3. The Wallet requests a fresh nonce for the wallet attestation nonce (wallet
   attestation)
4. The PID Provider generates a fresh nonce linked to the issuance session
5. The PID Provider returns the wallet attestation nonce to the Wallet
6. The Wallet performs a wallet attestation
   * optionally fetches a new wallet attestation from the Wallet Provider if not
     existent yet
   * generates a proof of possession (PoP) for the public key of the wallet
     attestation using its WSCD
7. The wallet sends the Pushed Authorization Request to the PID Provider;
   containing
   * the Wallet Provider's client_id
   * authorization_details for PID
   * a PKCE code_challenge
   * a wallet attestation and proof of possession
   * either scope or an authorization_details parameter requesting the PID
8. The PID Provider verifies the wallet attestation and its proof of possession
   and validates the certification status of the Wallet Solution on a trust list
9. The PID Provider returns a request_uri that is bound to the Pushed
   Authorization Request
10. The Wallet sends the Authorization Request; containing
    * the PAR request_uri
11. The PID Provider responds with the first step to start the eID process with
    the wallet app, e.g. the tcToken. Note that this is the direct HTTP Response
    to Step 14.
12. Further communication is exchanged to perform the eID process
13. The user provides the eID PIN to the wallet app.
14. Further communication is exchanged to perform the eID process
15. The eID process is finished and as a final step the Wallet sends a request
    to the PID Provider calling the refreshURL. From now on Wallet and PID
    Provider are using the TLS-PSK channel generated by the eID flow.
16. The PID Provider responds to the Wallet with an Authorization Response;
    containing
    * the auth code
17. The Wallet generates (and stores) a placeholder DPoP proof with a generic
  (not WSCD-bound) keypair to trigger an error response from the Token endpoint
  necessary to retrieve the `dpop_nonce`.
18. The Wallet sends a Token Request to the PID Provider, containing the
    placeholder DPoP proof JWT.
19. The PID Provider generates and stores a `dpop_nonce`.
20. The PID Provider responds with the expected error "use_dpop_nonce",
    containing the `dpop_nonce` to be used from now on in the DPoP nonce header.
21. The Wallet extracts and stores the `dpop_nonce`.
22. The Wallet now prepares the actual DPoP proof JWT for `dpop_pub` including
    the `dpop_nonce` and `iat`.
23. The Wallet hashes the prepared DPoP proof JWT, signs the hash with
    `dpop_priv` using the WSCD and includes the signature into the DPoP proof
    JWT
24. The Wallet sends a Token Request to the PID Provider; containing:
    * the auth code from Authorization Response
    * the PKCE code_verifier matching the code_challenge from Authorization
      Request
    * the wallet attestation and proof of possession
    * a DPoP key
25. The PID Provider generates and stores a fresh `dpop_nonce`.
26. The PID Provider matches the code, verifies the PKCE code_verifier to the
    previously received code_challenge, verifies the DPoP proof and verifies the
    wallet attestation. It then generates an access token bound to the DPoP key.
27. The PID Provider sends a Token Response; containing
    * DPoP-bound access token
    * a c_nonce
    * an authorization_details object, in case the authorization_details
      parameter was used in the Authorization Request.
    * a fresh `dpop_nonce` in the DPoP nonce header
28. The Wallet now prepares the actual DPoP proof JWT for `dpop_pub` including
    the `dpop_nonce` and `iat`.
29. The Wallet hashes the prepared DPoP proof JWT, signs the hash with
    `dpop_priv` using the WSCD and includes the signature into the DPoP proof
    JWT
30. The Wallet uses the device binding key (*device_pub*, *device_priv*) from
    the wallet attestation and signs the c_nonce using the Secure Element (WSCD)
31. The Wallet generates a new ephemeral keypair (*cre_eph_pub*,
    *cre_eph_priv*).
32. The Wallet creates the credential_response_encryption JSON object containing
    the following information:
    * a jwk containing the *cre_eph_pub*
    * the JWE alg parameter
    * the JWE enc parameter
33. The Wallet sends a Credential Request to the PID Provider; containing
    * the DPoP Header with the proof
    * the DpoP-bound access token
    * the credential_response_encryption object
    * the device binding key *device_pub* and the proof of possession
34. The Issuer validates the access token, the DPoP proof and the proof of
    possession
35. **(SD-JWT)** The PID Provider performs DH key exchange with the Wallet's
    public key *device_pub* and its private key *pp_priv*, generates a shared
    secret k and derives a MAC key
36. **(SD-JWT)** The PID Provider creates a user data JWT containing the eID
    data and secured with a HMAC
37. **(SD-JWT)** The PID Provider creates a certificate chain containing
    *device_pub* and signing with *pp_priv*
38. **(SD-JWT)** The PID Provider creates an encrypted JWT (JWE) using the
    values received in the `credential_response_encryption` object and adds
    (among others) the PID credential to the payload.
39. The PID Provider sends the Credential Response JWT; containing:
     * user data JWT with the certificate chain in x5c header
40. The Wallet decrypts the Credential Response JWT using the *cre_eph_priv* and
    retrieves the user data JWT.
41. The Wallet forwards both artifacts to the Secure Element.
42. The Secure Element performs DH key exchange with the PID Provider's public
    key *pp_pub* and its private key *device_priv*, generates a shared secret k,
    derives the MAC key and validates the authenticity of the user data JWT.
43. The Secure Element stores the user data and the x5c certificate chain
    securely and associates them to *device_pub*.
44. The Wallet returns a credential handle to the Wallet for future referencing
    the data for generating a PID.

### Credential Presentation

#### Credential Presentation Sequence Diagram

[User Journey: PID Presentation - Authenticated Channel - Secure
Element](../user_journeys/PID-AuthenticatedChannel-secureElement-presentation.png)

```plantuml
@startuml

'Ensure messages are not too wide
skinparam maxMessageSize 200
skinparam wrapWidth 300


'Macro for colored [TLS] block
!function tls()
!return "<color #118888>[TLS]</color>"
!endfunction

'Align text on arrows to center
skinparam sequenceMessageAlign center

'padding between boxes
skinparam BoxPadding 100

autonumber "<b>(000)"

title Presentation of PID for Option D12

actor u as "User\nOpenID Holder"
participant b as "Browser App\n(same device)"
participant v [
                Relying Party
                ----
                ""Long-term Key: (//rp_pub//, //rp_priv//)""
]
participant w as "User's EUDI Wallet Instance\n(eID-Client)"
participant wscd [
                  Wallet Secure Cryptographic Device (WSCD)
                  as Secure Element
                  ----
                  Long-term Key: (//device_pub//, //device_priv//)
                 ]

u --> b : browse to application
hnote over b #dfd: Screen: same_device_relying_party_start
b -> v : tls() HTTP GET <rp-website>

v -> v : generate ephemeral key pair (//rp_eph_pub//, //rp_eph_priv//)
note left: every RP in the ecosystem must always include ephemeral key in client metadata for ECDH-KA for MAC, however we need the ephemeral key anyway for encryption

v -> v : create OpenID4VP Authorization Request,\n sign with //rp_priv//,\n store under <request_uri>,\nbind to browser session
note left: Authorization Request includes:\n- presentation_definition\n- state\n- nonce\n- //rp_eph_pub// with PoP bound to client_id

v --> b : tls() HTTP 200 HTML containing wallet-link openid4vp://authorize?")\nclient_id=..&request_uri=<request_uri>
u --> b : action to start flow/launch wallet
b -> w : launch with wallet-link openid4vp://
note right #fc7: Potential security risk: Wallet app may be spoofed by malicious app
hnote over w #dfd: Screen: launch_wallet

u --> w : unlock wallet
note right: may be moved to later point in flow or removed, see notes.
hnote over w #dfd: Screen: unlock_wallet

w -> v : tls() HTTP GET <request_uri>
note right #fc7: Potential privacy risk: RP learns existance of wallet app and potentially identifying information (e.g., headers)
v --> w : tls() HTTP 200 <JWT-Secured Authorization Request>

'newpage

w -> w : validate Authorization Request JWT using //rp_pub//

u <--> w : user consent to present PID
hnote over w #dfd: Screen: consent_present_credential

note over w: determine if we need wallet attestation for presentation flows

w -> wscd: prepare credential presentation (//rp_eph_pub//, credential reference, claims to disclose, input for KB-JWT)

wscd -> wscd : perform DH key exchange with //rp_eph_pub// and //device_priv//,\n generate shared secret k\nand derive MAC key for PID

wscd -> wscd : generate ephemeral key pair for KB-JWT (//kb_eph_pub//, //kb_eph_priv//)

wscd -> wscd : create SD-JWT with user claims from secure storage, include //kb_eph_pub// and x5c with //device_pub//, HMAC using MAC key

note over wscd #AAFFAA: SD-JWT data is HMAC'd, not signed

wscd -> wscd: create the KB-JWT with input provided by the wallet app

wscd --> w: PID SD-JWT+KB-JWT

w -> w : create vp_token and presentation_submission PID SD-JWT+KB-JWT
w -> w : add mDL presentation according to <presentation_definition> with <nonce> to vp_token and presentation_submission
hnote over w #dfd: Screen: wallet_pin

'newpage

w -> v : tls() HTTP POST encrypted <Authorization Response(presentation_submission, vp_token, redirect_uri, state)
v -> v : perform DH key exchange with //rp_eph_priv// and //device_pub//,\n generate shared secret k\nand derive MAC key
v -> v : match session with <state> and verify <vp_token> PID with MAC key and //kb_eph_pub// and SD-JWT with ECDSA
v --> w : tls() HTTP 200 <redirect_uri with response_code>

w -> b : launch browser with <redirect_uri with response_code>
hnote over w #dfd: Screen: success_redirect

b -> v : tls() HTTP GET <redirect_uri with response_code>
v -> v : match response_code
v --> b : tls() HTTP 200 <HTML with continued UX flow>
hnote over b #dfd: Screen: same_device_relying_party_identified

@enduml
```

<!-- PDF-show
\elandscape
-->

#### Credential Presentation Step-by-Step Description

1. User browses to Relying Party (RP) website
2. Browser app on the user's device opens the RP website
3. RP generates a key pair to be used for ECDH key agreement for SD-JWT hmac'ing
4. RP generates an OpenID4VP Authorization Request and stores it under a
   `request_uri` (e.g., `https://rp.example.com/oidc/request/1234`);
   * The request is bound to the user's browser session
   * It is signed using a key bound to the RP's metadata that can be retrieved
     using the RP's client_id
   * It contains the ephemeral key for ECDH key agreement for SD-JWT hmacing
   * It contains RP's nonce and state
5. RP returns a HTML page to the browser containing a link to the wallet app
   (e.g.,
   `openid4vp://authorize?client_id=..&request_uri=https://rp.example.com/oidc/request/1234`)
6. The user clicks on the link
7. The RP website navigates to the custom scheme link to launch the wallet app
8. The user unlocks the wallet app (see notes below)
9. The wallet app retrieves the Authorization Request from the RP website
    (e.g., `https://rp.example.com/oidc/request/1234`)
10. The wallet app receives the Authorization Request
11. The wallet app validates the Authorization Request using the RP's public key
    * Was the signature valid and the key bound to the RP's metadata?
    * **Security:** This ensures that the Authorization Request was not tampered
      with; it does not ensure that the party that sent the Authorization
      Request is the RP.
12. The user gives consent to present the PID to the RP.
13. The wallet app queries the Secure Element for the PID providing:
    * the credential handle referring to the PID credential,
    * the ephemeral RP key *rp_eph_pub* from the RP's authorization request,
    * a list of claims,
    * input for the KB-JWT (directly or hash)
14. The Secure Element performs DH key exchange with the ephemeral RP public key
    *rp_eph_pub* and the private part of DeviceKey *device_priv* , generates a
    shared secret k and derives a MAC key for the deviceAuth
15. The Secure Element generates an ephemeral key pair for the KB-JWT
    (*kb_eph_pub*, *kb_eph_priv*).
16. The Secure Element creates the issuer-signed part of the SD-JWT (using the
    MAC key) containing:
    * stored x5c header
    * stored user claims
    * *kb_eph_pub* as cnf claim
17. The Secure Element creates a KB-JWT payload containing the nonce, audience,
    and a hash of the SD-JWT and the selected disclosures and signs it using
    *kb_eph_priv*.
18. The Secure Element sends the SD-JWT and KB-JWT to the wallet app.
19. The wallet app creates a VP token and a presentation submission from the
    received SD-JWT PID.
20. Optional: The wallet app can add the mDL presentation using the nonce and
    ECDSA according to the presentation definition
21. The wallet app sends the VP token and presentation submission to the RP
    (encrypted to the RP's public key *rp_eph_pub*).
22. The RP performs DH key exchange with the PID deviceKey public key, generates
    a shared secret k and derives a MAC key.
23. The RP finds a session with the state and verifies the PID in the VP token
    with the MAC key.
24. The RP generates a response code, attaches it to the same session, and
    returns it to the wallet app via a redirect_uri.
25. The wallet app launches the browser with the redirect_uri and response code.
26. The browser sends the redirect_uri and response code to the RP.
27. The RP matches the response code with the browser session (i.e., ensures
    that the session tied to the browser session matches the one tied to the
    response code).
28. The RP considers the user to be identified in the session context and
    continues the UX flow.

## Extensions to the Protocols

This section defines extensions to the protocols required to implement this flow
(Option D).

### Issuer Session Endpoint (at the PID Provider)

Note that this extension is the same across multiple flows.

This endpoint is used by the Wallet to obtain `session_id` from the PID Provider
that is used to bind PoPs to the session and prove their freshness. Support for
this endpoint is REQUIRED.

To fetch the `session_id`, the Wallet MUST send an HTTP request using the POST
method and the `application/json` media type. The PID Provider MUST return the
HTTP Status Code 200 and a `session_id` parameter defined below.

* `session_id`: REQUIRED. String that is a unique session identifier, chosen as
  a cryptographically random nonce with at least 128 bits of entropy.

Communication with the Session Endpoint MUST utilize TLS.

Below is a non-normative example of a request to a Session Endpoint:

```http
POST /session_endpoint HTTP/1.1
Host: server.example.com
Content-Type: application/json
```

Below is a non-normative example of a response from a Session Endpoint:

```http
HTTP/1.1 200 OK
Content-Type: application/json
Cache-Control: no-store

{
  "session_id": "iOiJSUzI1NiIsInR"
}
```

### OpenID4VCI Credential Issuer Metadata

Note that this extension is the same across multiple flows.

This document defines the following additional Credential Issuer Metadata
parameters:

* `session_endpoint`: REQUIRED. URL of the Credential Issuer's Session Endpoint,
  as defined in a previous section. This URL MUST use the `https` scheme and MAY
  contain port, path, and query parameter components.

## Usability Considerations

### Initialization

* The Wallet needs to be initialized on a mandatory basis
* During initialization, the user must set a Wallet PIN
* Having only one Wallet PIN has the advantage that the same PIN (eID PIN
  excluded) could probably also be used for other credentials (also at the same
  LoA). From a UX perspective, this would be a great added value for users.
* The wallet can only be used after successful initialization

### Issuance

* Credential catalogue should inform users in advance of what is required for
  the successful issuance of the PID and what steps follow
* For reasons of transparency and to increase trust, PID Provider should provide
  sufficient information (metadata) for the consent screen. This allows users to
  learn everything relevant e.g. about the PID Provider itself
* eID process is integrated in Wallet. No context switch to the AusweisApp is
  required
* Physical ID card is required for issuing the PID credential
* Online-Ausweisfunktion must be activated
* eID PIN must be set by the user (replacement of the Transport PIN) and be
  known to them so that they can successfully confirm the process
* User must confirm the process with the eID PIN and the Wallet PIN. The
  distinction between the two PINs should be easier for users than the
  distinction between eID PIN and PID PIN
* User can have a PID credential derived from the eID on several end devices at
  the same time

### Presentation

* Offline support for PID representation
* Relying Party should inform users in advance of what is required for the
  process to be completed successfully and what steps follow
* It needs to be clarified whether the wallet app needs to be unlocked in this
  flow and how this should be implemented.
* For reasons of transparency and to increase trust, Relying Parties should
  provide sufficient information (metadata) for the consent screen. This allows
  users to learn everything relevant e.g. about the relying party itself,
  privacy and data retention
* To be able to display the user's data values for the requested attributes on
  the consent screen, the wallet must first be unlocked with the wallet PIN (to
  access data from the SE)
* The user only needs to enter the Wallet PIN for each PID presentation
* Physical ID card is not required for PID presentation
* It must be ensured that users return to the correct tab in the correct browser
  in order to continue the process or know how to get there manually if
  necessary (especially for iOS devices, if the process was not started in the
  default browser)

## Privacy considerations

* What is the information that is shared with the wallet provider backend? Is it
  okay if the backend learn which PID Providers are used?
* Multiple device keys must be used to ensure unlinkability across issuers
  and/or across relying parties
* Selective Disclosure is achieved by ensuring that the credential contains only
  those data items (claims) that are requested by the RP.

## Security considerations

* eID Process in the browser means that a remote cooperating party can complete
  the eID retrieval for the attacker (easier than when used via app)
