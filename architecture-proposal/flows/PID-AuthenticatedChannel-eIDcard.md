<!-- PDF-show
\newpage
-->

# PID Option B: Authenticated Channel with eID Card

## Basic Idea

This flow describes a flow that issues PID credentials in ISO mdoc / SD-JWT VC
format on-demand when the presentation request is received. It uses the
OpenID4VCI protocol for issuance and the OpenID4VP protocol for presentation.
The PID credentials are HMAC'd by the PID Provider for the specific transaction.

The Wallet Instance is not trusted for the management of the keys, hence
majority of the signing is performed by the PID Provider.

## Lifecycle

The credential is created on-demand by the PID Provider, i.e., a new credential
is created for every transaction. The credential is not stored in the wallet. It
is bound to the specific transaction through not only the nonce and audience,
but also the symmetric key used for HMAC'ing, which is derived from ephemeral
keys of the RP and PID Provider.

## Credential formats

Two solutions are described:

- **ISO mdoc:** The ISO mdoc credential format is used with:
    - issuerAuth as issuer data authentication, a COSE_Sign1 signature over the
    MobileSecurityObject (see ISO 23220-4 7.1.3.4.2.1) signed by the PID
    Provider
        - no signed hashes are transmitted, i.e., `digestAlgorithm` and
      `valueDigests` are omitted
    - deviceMAC as mdoc authentication method, a COSE_Mac0 MAC over the
    deviceAuthentication data (see ISO 18013-5 9.1.3.5) signed by the PID
    Provider
        - containing the PID data
- **SD-JWT VC:** The SD-JWT VC credential format is used with:
    - SD-JWT issued by the PID Provider, a JOSE JWS with MAC key derived from
    ECDH* and signed by the PID Provider
        - containing the hashes of PID data
        - the Wallet provides an ephemeral, asymmetric key to the PID Provider
      for the key binding
    - KB-JWT issued by the Wallet, a JOSE JWS using digital signature and signed
    by the Wallet
        - containing nonce and audience of the Relying Party
        - containing a hash of the SD-JWT and the selected disclosures
    - the Disclosures
        - containing the PID data

The establishment of the authenticated channel is defined:

- for mdoc, in Chapter 9.1.3.5 in ISO 18013-5, in the mdoc MAC Authentication
  section
- for SD-JWT, the IETF Draft for [Designated Verifier
  Signatures](https://datatracker.ietf.org/doc/draft-bastian-dvs-jose/) defines
  a JOSE algorithm to be used

## Cryptographic Formats

Long-Term keys:

- **PID Provider has long-term key pair** $(pp\_pub, pp\_priv)$ is used to sign
  over the ephemeral issuer key, authenticating it for credential issuance
- **Relying Party has long-term key pair** $(rp\_pub, rp\_priv)$ which is used
  to sign over the authorization request, authenticating its contents
- **Wallet Instance has long-term device bound key pair** $(device\_pub,
  device\_priv)$ which is used to generate proof of possession of wallet
  attestation

Transaction-specific keys:

- **Relying Party generates ephemeral key pair** $(rp\_eph\_pub, rp\_eph\_priv)$
  which is used as contribution to HMAC key
- **PID Provider generates ephemeral key pair** $(pp\_eph\_pub, pp\_eph\_priv)$
  which is used as contribution to HMAC key
- **Wallet generates ephemeral key pair** $(kb\_eph\_pub, kb\_eph\_priv)$ which
  is used to sign KB-JWT (SD-JWT)
- **Wallet generates ephemeral key pair** $(dpop\_eph\_pub, dpop\_eph\_priv)$
  which is used to sign DPoP proof JWT

Artifacts mdoc:

- PID Provider creates **issuerAuth**: $\text{issuerAuth} :=
  \text{sign}(pp\_eph\_pub, x5chain)_{pp\_priv}$
- **Key for authenticating mdoc:** $hmac\_key := \text{ecdh}(rp\_eph\_pub,
  pp\_eph\_priv)$
- PID Provider creates **deviceAuth**: $\text{deviceAuth} :=
  \text{hmac}(\mathit{eID\_data}, \mathit{SessionTranscript})_{hmac\_key}$

Artifacts SD-JWT VC:

- PID Provider creates **certificate for ephemeral issuer key**: $x5c\_header :=
  \text{sign}(pp\_eph\_pub)_{pp\_priv}$
- **Key for authenticating SD-JWT:** $hmac\_key := \text{ecdh}(rp\_eph\_pub,
  pp\_eph\_priv)$
- PID Provider creates **SD-JWT**: $sd\_jwt := \text{hmac}(x5c\_header,
  \mathit{eID\_data}, kb\_eph\_pub)_{hmac\_key}$
- Wallet creates **KB-JWT**: $kb\_jwt := \text{sign}(nonce, audience,
  \text{hash}(sd\_jwt, disclosures))_{kb\_eph\_priv}$

<!-- PDF-show
\newpage
-->

### Dependencies

*TODO: May want to expand to include metadata.*

For mdoc:

```plantuml
digraph G {

  subgraph cluster_pid_provider {
    style=filled
    color=lightblue
    label="PID Provider"
    pp [label="🗝 (pp_priv, pp_pub)"]
    pp_eph [label="🗝 (pp_eph_priv, pp_eph_pub)"]
    pp -> pp_eph [label="sign (issuerAuth)"]
    deviceAuth [label="deviceAuth(eID data, SessionTranscript(nonce, audience))"]
    shared_secret_k [label="🗝 shared secret K"]
    sd_jwt_hmac_key [label="🗝 hmac key"]
  }

  subgraph cluster_rp {
    style=filled
    color=lightyellow
    label="Relying Party"
    rp [label="🗝 (rp_priv, rp_pub)"]
    rp_eph [label="🗝 (rp_eph_priv, rp_eph_pub)"]
    rp -> rp_eph [label="sign (Auth. Request)"]
  }

  pp_eph -> shared_secret_k [label="ecdh-ka"]
  rp_eph -> shared_secret_k [label="ecdh-ka"]
  shared_secret_k -> sd_jwt_hmac_key [label="key derivation"]
  sd_jwt_hmac_key -> deviceAuth [label="hmac"]
}
```

<!-- PDF-show
\newpage
-->

For SD-JWT VC:

```plantuml
digraph G {

  subgraph cluster_pid_provider {
    style=filled
    color=lightblue
    label="PID Provider"
    pp [label="🗝 (pp_priv, pp_pub)"]
    pp_eph [label="🗝 (pp_eph_priv, pp_eph_pub)"]
    pp -> pp_eph [label="sign"]
    user_data [label="eID data"]
    shared_secret_k [label="🗝 shared secret K"]
    sd_jwt_hmac_key [label="🗝 hmac key"]
  }

  kb_eph [label="🗝 (kb_eph_priv, kb_eph_pub)"]

  subgraph cluster_rp {
    style=filled
  color=lightyellow
    label="Relying Party"
    rp [label="🗝 (rp_priv, rp_pub)"]
    rp_eph [label="🗝 (rp_eph_priv, rp_eph_pub)"]
    rp -> rp_eph [label="sign (Auth. Request)"]
  }

  subgraph cluster_wallet {
    style=filled
    color=lightgreen
    label="Wallet"
    nonce_audience [label="nonce, audience"]
    dpop [label="DPoP proof (dpop_nonce)"]
    dpop_key [label="🗝 (dpop_eph_priv, dpop_eph_pub)"]
  }

  pp_eph -> shared_secret_k [label="ecdh-ka"]
  rp_eph -> shared_secret_k [label="ecdh-ka"]
  shared_secret_k -> sd_jwt_hmac_key [label="key derivation"]
  sd_jwt_hmac_key -> kb_eph [label="hmac (SD-JWT)"]
  kb_eph -> nonce_audience [label="sign (KB-JWT)"]
  sd_jwt_hmac_key -> user_data [label="hmac (SD-JWT)"]
  dpop_key -> dpop [label="sign"]
}
```

<!-- PDF-show
\newpage
\blandscape
-->

## Wallet Activation

To initialize the wallet, the Wallet obtains wallet attestation as defined in
[Wallet Attestation](Wallet-Attestation.md).

## [Sequence Diagram] On-demand PID Issuance

[User Journey: PID Presentation - Authenticated Channel - eID
Card](../user_journeys/PID-AuthenticatedChannel-eIDcard-presentation.png)

```plantuml
@startuml

'Ensure messages are not too wide
skinparam maxMessageSize 200
skinparam wrapWidth 300

'Macro for colored [TLS] block
!function tls()
!return "<color #118888>[TLS]</color>"
!endfunction

'Align text on arrows to center
skinparam sequenceMessageAlign center

'padding between boxes
skinparam BoxPadding 100

autonumber "<b>(000)"

title PID presentation over OpenID4VP and On-the-fly PID Issuance over OpenID4VCI with SD-JWT

actor u as "User\nOpenID Holder"
participant b as "Browser App\n(same device)"
participant v [
                Relying Party
                ----
                ""Long-term Key: (//rp_pub//, //rp_priv//)""
]
participant w as "User's EUDI Wallet Instance\n(eID-Client)"
participant i  [
                PID Provider
                (eService+eID Server)
                ----
                ""Long-term Key: (//pp_pub//, //pp_priv//)""
            ]


u --> b : browse to application
hnote over b #dfd: Screen: same_device_relying_party_start
b -> v : tls() HTTP GET <rp-website>

v -> v : generate ephemeral key pair (//rp_eph_pub//, //rp_eph_priv//)
note left: every RP in the ecosystem must always include ephemeral key in client metadata for ECDH-KA for MAC, however we need the ephemeral key anyway for encryption

v -> v : create OpenID4VP Authorization Request,\n sign with //rp_priv//,\n store under <request_uri>
note left: Authorization Request includes:\n- presentation_definition\n- purpose\n- state\n- nonce\n- //rp_eph_pub// with PoP bound to client_id\n- response_uri

v -> v : generate new browser session <color:#909>session_id</color> and bind the authorization request to it

v -> b : tls() HTTP 200 HTML containing wallet-link openid4vp://authorize?")\nclient_id=..&request_uri=<request_uri>\nSet-Cookie: sid=<color:#909>session_id</color>
u --> b : action to start flow/launch wallet
b -> w : launch with wallet-link openid4vp://
note right #fc7: Potential security risk: Wallet app may be spoofed by malicious app
hnote over w #dfd: Screen: launch_wallet

u --> w : unlock wallet
note right: may be moved to later point in flow or removed, see notes.
hnote over w #dfd: Screen: unlock_wallet

'newpage

w -> v : tls() HTTP GET <request_uri>
note right #fc7: Potential privacy risk: RP learns existence of wallet app and potentially identifying information (e.g., headers)
v -> w : tls() HTTP 200 <JWT-Secured Authorization Request>

w -> w : validate Authorization Request JWT using //rp_pub//

u <--> w : user consent to present PID to Relying Party for given purpose
hnote over w #dfd: Screen: consent_present_credential

group PID provisioning
  note over w,i: PID Issuer and EUDI Wallet have inherent trust relationship, metadata may be pre-configured or retrieved

  w -> w : Wallet fetches fresh wallet attestation from backend
  w -> i : tls() HTTP POST </session_endpoint> wallet attestation nonce
  i -> i : generate and store nonce
  i -> w : tls() HTTP 200 <wallet attestation nonce>
  w -> w : sign wallet attestation PoP JWT (incl. wallet attestation nonce) with //device_priv//
  w -> i : tls() HTTP POST PAR (PKCE code_challenge, wallet attestation JWT, wallet attestation PoP JWT, redirect_uri, either scope or authorization_details)
  i -> i : verify wallet attestation & PoP \ncheck Wallet Provider solution status on trust list
  i -> w : tls() HTTP 200 request_uri
  note right : Attestation guarantees with high certainty that Wallet is trustworthy and not manipulated
  w -> i : tls() HTTP GET <OpenID4VCI Authorization Request(request_uri)>

'newpage
  hnote over w #dfd: Screen: eid_start

  group Read eID or Smart eID acc. to BSI TR-03130
    i --> w : tls() HTTP 200 starting the eID Process
    w <-> i : eID Process
    u <--> w : <eID-PIN>
    hnote over w #dfd: Screen: eid_pin
    w <-> i : eID Process
    w -> i : tls() HTTP GET finishing the eID process with refreshUrl
    hnote over w #dfd: Screen: eid_nfc_data
  end

  i --> w : tls() HTTP 200 Authorization Response (code)

  group Generate initial DPoP nonce
    w -> w : generate ephemeral DPoP key pair //dpop_eph_pub//, //dpop_eph_priv//

    w -> w : generate placeholder DPoP proof using ephemeral DPoP key pair //dpop_eph_pub//, //dpop_eph_priv//

    w -> i : tls() HTTP POST <Token Request(DPoP Header with placeholder proof, authorization_code, PKCE code_verifier)>
    i -> i : generate and store dpop_nonce
    i -> w : tls() HTTP 400 <Bad Request (DPoP nonce Header with dpop_nonce, error: "use_dpop_nonce")>

    note left : The Wallet should check at this point, whether the Token Endpoint delivered the expected error and nonce. If not, this needs to be handled (retry or abort gracefully).
    w -> w: store dpop_nonce
  end

  w -> w: prepare DPoP proof JWT with //dpop_eph_pub//, dpop_nonce, iat and sign with //dpop_eph_priv//

  w -> i : tls() HTTP POST Token Request(code, code_verifier, DPoP header)
  i -> i : generate and store dpop_nonce
  i -> i : lookup authorization code\ngenerate Token Response with DPoP-bound access token\nverify PKCE challenge\nverify DPoP proof
  i --> w : tls() HTTP 200 Token Response(DPoP nonce header, DPoP-bound access_token, c_nonce, optional authorization_details)
  w -> w : generate credential response encryption key pair (//cre_eph_pub//, //cre_eph_priv//)
  w -> w : create credential_response_encryption object with jwk containing //cre_eph_pub//
  w -> w: prepare DPoP proof JWT with //dpop_eph_pub//, dpop_nonce, iat and sign with //dpop_eph_priv//

'newpage

  alt #ddf B.1.1: ISO mdoc
    w -> w : calculate SessionTranscript (mDocGeneratedNonce, client_id, responseUri, nonce)
    w -> i : tls() Credential Request (DPoP header with proof, //rp_eph_pub//, credential_response_encryption object, <color blue>SessionTranscript</color>)
    note right: -//rp_eph_pub// is sent in `verifier_pub` parameter\n-SessionTranscript is sent in `session_transcript` parameter\n-no `proof`/`proofs` parameters
    i -> i : generate device key pair (//pp_eph_pub//, //pp_eph_priv//),\n create issuerAuth with //pp_eph_pub// and w/o data items\n and sign with //pp_priv//

  else #dfd B.1.2: SD-JWT VC
    w -> w : generate ephemeral key pair for KB-JWT (//kb_eph_pub//, //kb_eph_priv//) and sign c_nonce
    w -> i : tls() Credential Request (DPoP header with proof, //rp_eph_pub//, credential_response_encryption object, <color blue>//kb_eph_pub//</color>)
    note right: //rp_eph_pub// is sent in `verifier_pub` parameter
    i -> i : generate issuer key pair (//pp_eph_pub//, //pp_eph_priv//),\n create certificate for chain with //pp_eph_pub// as x5c header and sign with //pp_priv//
  end

'newpage

  i -> i : perform DH key exchange with //rp_eph_pub// and //pp_eph_priv// / //pp_eph_priv//,\n generate shared secret k\nand derive MAC key for PID
  note over i: ECDH-MAC not specified for JOSE yet
  note over i #AAFFAA: SD-JWT is HMAC'd, not signed

  alt #ddf B.1.1: ISO mdoc
    i -> i : create deviceAuth using MAC key with eID data and SessionTranscript
    i -> i : generate encrypted credential response JWT using the values received in the credential_response_encryption object
    i --> w : tls() HTTP 200 JWT(Credential Response(<color blue>mdoc</color>))
    w -> w : decrypt credential response JWT and retrieve PID
  else #dfd B.1.2: SD-JWT VC
    i -> i : - create SD-JWT with eID data, include //kb_eph_pub// and x5c with //pp_eph_pub//, HMAC using MAC key
    i -> i : generate encrypted credential response JWT using the values received in the credential_response_encryption object
    i --> w : tls() HTTP 200 JWT(Credential Response(<color blue>SD-JWT</color>))
    note right: SD-JWT contains no disclosures\nand no KB-JWT
    w -> w : decrypt credential response JWT and retrieve PID
    w -> w : create KB-JWT payload with nonce, audience, and hash of SD-JWT and selected disclosures and sign with //kb_eph_priv//
  end

end

w -> w : create vp_token and presentation_submission
w -> w : add mDL presentation according to <presentation_definition> with <nonce> to vp_token and presentation_submission
note left #AAFFAA: Wallet may add presentations with keys under its own control as the \ncommunication channel between Relying Part and PID Provider is not E2EE


w -> v : tls() HTTP POST encrypted <Authorization Response(presentation_submission, vp_token, state)>

v -> v : look up state in existing sessions\ncreate & store response_code for session

v --> w : tls() HTTP 200 <redirect_uri incl. response_code>

'newpage

w -> b : launch browser with <redirect_uri with response_code>
hnote over w #dfd: Screen: success_redirect

b -> v : tls() HTTP GET <redirect_uri with response_code>\nCookie: sid=<color:#909>session_id</color>

v -> v : look up session with <color:#909>session_id</color> and match response_code
v -> v : perform DH key exchange with //rp_eph_priv// from session and //pp_eph_pub//,\n generate shared secret k\n derive MAC key

alt #ddf B.1.1: ISO mdoc
  v -> v : verify contents of <vp_token>:\n- verify mdoc issuerAuth/deviceAuth PID with MAC key\n- calculate and validate correct SessionTranscript
else #dfd B.1.2: SD-JWT VC
  v -> v : verify contents of <vp_token>:\n- verify SD-JWT PID with MAC key\n- verify KB-JWT with //kb_eph_pub// from SD-JWT\n- validate nonce and audience from KB-JWT
end

v --> b : tls() HTTP 200 <HTML with continued UX flow>
hnote over b #dfd: Screen: same_device_relying_party_identified

@enduml

```

<!-- PDF-show
\elandscape
-->

## [Step-by-Step Description] On-demand PID Issuance

Note: While certain assumptions about session management of the Relaying Party
are made here, the concrete implementation is considered out of scope for this
document. The usual security considerations for web session management apply.

1. User browses to Relying Party (RP) website
2. Browser app on the user's device opens the RP website
3. RP generates a key pair to be used for ECDH key agreement for SD-JWT HMAC'ing
4. RP generates an OpenID4VP Authorization Request and stores it under a
   `request_uri` (e.g., `https://rp.example.com/oidc/request/1234`);
   - The request is bound to the user's browser session
   - It is signed using a key bound to the RP's metadata that can be retrieved
     using the RP's client_id
   - It contains the ephemeral key for ECDH key agreement for SD-JWT HMAC'ing
   - It contains RP's nonce and state
   - It contains the RP's response_uri endpoint for sending the Authorization
     Response over POST
5. RP generates a new browser session and binds the generated Authorization
   Request to it
6. RP returns an HTML page to the browser containing a link to the wallet app
   (e.g.,
   `openid4vp://authorize?client_id=..&request_uri=https://rp.example.com/oidc/request/1234`);
   a cookie with the browser session id is set
7. The user clicks on the link
8. The RP website navigates to the custom scheme link to launch the wallet app
9. The user unlocks the wallet app (see notes below)
10. The wallet app retrieves the Authorization Request from the RP website
    (e.g., `https://rp.example.com/oidc/request/1234`)
11. The wallet app receives the Authorization Request
12. The wallet app validates the Authorization Request using the RP's public key
    - Was the signature valid and the key bound to the RP's metadata?
    - **Security:** This ensures that the Authorization Request was not tampered
      with; it does not ensure that the party that sent the Authorization
      Request is the RP.
13. The Wallet displays information about the identity of the Relying Party and
    the purpose, the user gives consent to present the PID.
14. The Wallet fetches fresh wallet attestation from the Wallet Provider
    backend.
15. The Wallet requests a fresh nonce for the wallet attestation nonce from the
    PID Provider (wallet attestation nonce).
16. The PID Provider generates a fresh nonce linked to the issuance session.
17. The PID Provider returns the wallet attestation nonce to the Wallet.
18. The Wallet generates a Wallet Attestation PoP and signs it with
    *device_priv*; containing
    - audience
    - expiration time
    - wallet attestation nonce
19. The wallet sends the Pushed Authorization Request to the PID Provider;
    containing
    - PKCE code_challenge
    - wallet attestation + PoP
    - redirect_uri
    - either scope or an authorization_details parameter requesting the PID
20. The PID Provider verifies the wallet attestation and its proof of possession
    and validates the certification status of the Wallet Solution on a trust
    list.
21. The PID Provider returns a request_uri that is bound to the Pushed
    Authorization Request.
22. The Wallet sends the Authorization Request; containing
    - the PAR request_uri
23. The PID Provider responds with the first step to start the eID process with
    the wallet app, e.g. the tcToken. Note that this is the direct HTTP Response
    to Step 14.
24. Further communication is exchanged to perform the eID process
25. The user provides the eID PIN to the wallet app.
26. Further communication is exchanged to perform the eID process
27. The eID process is finished and as a final step the Wallet sends a request
    to the PID Provider calling the refreshURL. From now on Wallet and PID
    Provider are using the TLS-PSK channel generated by the eID flow.
28. The PID Provider responds to the Wallet with an Authorization Response;
    containing
    - the auth code
29. The Wallet generates an ephemeral DPoP key pair _dpop_eph_pub_,
    _dpop_eph_priv_.
30. The Wallet generates a placeholder DPoP proof JWT using the ephemeral DPoP
    key pair to trigger an error response from the Token endpoint necessary to
    retrieve the `dpop_nonce`.
31. The Wallet sends a Token Request to the PID Provider, containing the
    placeholder DPoP proof JWT.
32. The PID Provider generates and stores a `dpop_nonce`.
33. The PID Provider responds with the expected error "use_dpop_nonce",
    containing the `dpop_nonce` to be used from now on in the DPoP nonce header.
34. The Wallet extracts and stores the `dpop_nonce`.
35. The Wallet now prepares the actual DPoP proof JWT for `dpop_eph_pub`
    including the `dpop_nonce` and `iat` signed with *dpop_eph_priv*.
36. The Wallet sends a Token Request to the PID Provider; containing:
    - the authorization_code from Authorization Response
    - the PKCE code_verifier matching the code_challenge from Authorization
      Request
    - the DPoP Header
37. The PID Provider generates and stores a `dpop_nonce`.
38. The PID Provider matches the authorization_code and verifies the PKCE
    code_verifier to the previously received code_challenge. It then generates
    an access token bound to the DPoP key.
39. The PID Provider sends a Token Response; containing
    - DPoP-bound access token
    - a c_nonce
    - an authorization_details object, in case the authorization_details
      parameter was used in the Authorization Request
    - a fresh `dpop_nonce` in the DPoP nonce header
40. The Wallet generates a new ephemeral keypair (*cre_eph_pub*,
    *cre_eph_priv*).
41. The Wallet creates the `credential_response_encryption` JSON object
    containing the following information:
    - a jwk containing the *cre_eph_pub*
    - the JWE alg parameter
    - the JWE enc parameter
42. The Wallet prepares the DPoP proof JWT for `dpop_eph_pub` including the
    `dpop_nonce` and `iat` signed with *dpop_eph_priv*.
43. **(mdoc)** The Wallet calculates the SessionTranscript according to
    ISO-18013-7 Annex B.4.4 from mDocGeneratedNonce, client_id, responseUri,
    nonce. The final result is a SHA-256 hash, thus not revealing the client_id
    and ResponseUri to the PID Provider.
44. **(mdoc)** The Wallet send a Credential Request; containing
    - DPoP header with proof
    - sessionTranscript
    - the ephemeral RP key *rp_eph_pub* from the RP's Authorization Request
    - the `credential_response_encryption` object
    - it does not contain a "proof"
45. **(mdoc)** The PID Provider generate an ephemeral DeviceKey pair
    (*pp_eph_pub*,*pp_eph_priv*) and creates the mdoc issuerAuth (MSO) with the
    public part of DeviceKey and without the IssuerSignedItems and signs it with
    *pp_priv*.
46. **(SD-JWT)** The Wallet generates an ephemeral key pair for the KB-JWT
    (*kb_eph_pub*, *kb_eph_priv*) and signs the nonce
47. **(SD-JWT)** The wallet app sends the Credential Request to the PID
    Provider; containing
    - the DPoP header with proof
    - the ephemeral RP key *rp_eph_pub* from the RP's authorization request,
    - the `credential_response_encryption` object
    - the ephemeral KB-JWT key *kb_eph_pub*
48. **(SD-JWT)** The PID Provider generates the ephemeral issuer key pair
    (*pp_eph_pub*, *pp_eph_priv*) and creates a certificate for the chain with
    *pp_eph_pub* as x5c header and signs it with *pp_priv*.
49. The PID Provider performs DH key exchange with the ephemeral RP public key
    *rp_eph_pub* and the ephemeral issuer private key *pp_eph_priv*, generates a
    shared secret k and derives a MAC key for the PID.
50. **(mdoc)** The PID Provider creates the mdoc deviceAuth (using the MAC key)
    containing
    - the user claims
    - the OpenID4VP SessionTranscript(mdocGeneratedNonce, client_id,
      responseUri, nonce)
51. **(mdoc)** The PID Provider creates an encrypted JWT (JWE) using the values
    received in the `credential_response_encryption` object and adds (among
    others) the PID credential to the payload.
52. **(mdoc)** The PID Provider sends the Credential Response JWT; containing:
    - PID as mdoc
53. **(mdoc)** The Wallet decrypts the Credential Response JWT using the
    *cre_eph_priv* and retrieves the PID.
54. **(SD-JWT)** The PID Provider creates the issuer-signed part of the SD-JWT
    (using the MAC key) containing
    - eID as the user claims
    - *kb_eph_pub* as cnf claim
55. **(SD-JWT)** The PID Provider creates an encrypted JWT (JWE) using the
    values received in the `credential_response_encryption` object and adds
    (among others) the PID credential to the payload.
56. **(SD-JWT)** The PID Provider sends the Credential Response JWT; containing:
    - PID as SD-JWT VC
57. **(SD-JWT)** The Wallet decrypts the Credential Response JWT using the
    *cre_eph_priv* and retrieves the PID.
58. **(SD-JWT)** The Wallet creates the header and payload for the KB-JWT from
    audience, nonce, and the hash of SD-JWT and selected disclosures and signs
    it with its generated *kb_eph_priv*. The Wallet appends the KB-JWT to the
    SD-JWT.
59. The wallet app creates a VP token and a presentation submission from the
    received SD-JWT PID.
60. Optional: The wallet app can add further presentations with keys under its
    own control as the communication channel between Relying Part and PID
    Provider is not E2EE
61. The wallet app sends the VP token and presentation submission to the RP
    (encrypted to the RP's public key *rp_eph_pub*).
62. The RP finds a session with the state and generates a response_code for this
    session
63. The RP returns the redirect_uri with the response_code to the wallet app
64. The wallet app launches the browser with the redirect_uri and response_code.
65. The browser sends the redirect_uri and response code to the RP, attaching
    the browser session id as a cookie.
66. The RP looks up whether there exists a session with the session id from the
    cookie and a matching response_code
67. Using the data from the session the RP performs DH key exchange with the PID
    Provider's public key, generates a shared secret k and derives a MAC key.
68. **(mdoc)** The RP verifies the PID in the VP token with the MAC key and
    verifies the SessionTranscript calculated from nonce, mDocGeneratedNonce,
    client_id, response_uri.
69. **(SD-JWT)** The RP verifies the SD-JWT PID in the VP token with the MAC
    key, verifies the KB-JWT using the *kb_eph_pub* in the SD-JWT, and verifies
    the nonce and audience in the KB-JWT
70. The RP considers the user to be identified in the session context and
    continues the UX flow.

## Extensions to the Protocols

This section defines extentions to the protocols required to implement this flow
(Option B).

In this document, the term `RP` has been used, but this since this section is an
extension to an OpenID4VCI protocol, a term from that specification is being
used, which is `verifier`.

### Issuer Session Endpoint (at the PID Provider)

Note that this extension is the same across multiple flows.

This endpoint is used by the Wallet to obtain `session_id` from the PID Provider
that is used to bind PoPs to the session and prove their freshness. Support for
this endpoint is REQUIRED.

To fetch the `session_id`, the Wallet MUST send an HTTP request using the POST
method and the `application/json` media type. The PID Provider MUST return the
HTTP Status Code 200 and a `session_id` parameter defined below.

- `session_id`: REQUIRED. String that is a unique session identifier, chosen as
  a cryptographically random nonce with at least 128 bits of entropy.

Communication with the Session Endpoint MUST utilize TLS.

Below is a non-normative example of a request to a Session Endpoint:

```http
POST /session_endpoint HTTP/1.1
Host: server.example.com
Content-Type: application/json

```

Below is a non-normative example of a response from a Session Endpoint:

```http
HTTP/1.1 200 OK
Content-Type: application/json
Cache-Control: no-store

{
  "session_id": "iOiJSUzI1NiIsInR"
}
```

### OpenID4VCI Credential Issuer Metadata

Note that this extension is the same across multiple flows.

This document defines the following additional Credential Issuer Metadata
parameters:

- `session_endpoint`: REQUIRED. URL of the Credential Issuer's Session Endpoint,
  as defined in a previous section. This URL MUST use the `https` scheme and MAY
  contain port, path, and query parameter components.

### OpenID4VCI Credential Request and Response for PID Issuance in mdoc Credential Format using Authenticated Channel

Note that this section is the same as in Option B'.

Credential Format identifier is `mso_mdoc_authenticated_channel`.

The following parameters are defined in addition to those defined for the
Credential Format `mso_mdoc` in Annex A.2 of OpenId4VCI:

- `session_transcript`: REQUIRED. String that is a base64url encoded
  SessionTranscriptBytes as defined in Section 9.1.5.1 of ISO 18013-5.
- `verifier_pub`: REQUIRED. A JSON object as defined in Section 2 of RFC7591. It
  contains the ephemeral RP key *rp_eph_pub* from the RP's Authorization
  Request.

`proof` or `proofs` parameter MUST NOT be present.

`credential_response_encryption` parameter MUST be present. Note: The wallet can
find out about the supported cryptographic algorithms by using the Issuer's
metadata parameter `credential_response_encryption`.

DPoP-bound Access token MUST be present.

Below is a non-normative example of a Credential Request during the PID issuance
in ISO mso Credential Format over Authenticated Channel:

```json
POST /credential HTTP/1.1
Host: server.example.com
Content-Type: application/json
Authorization: DPoP czZCaGRSa3F0MzpnWDFmQmF0M2JW
DPoP: ey…

{
  "format": "mso_mdoc_authenticated_channel",
  "doctype": "org.iso.18013.5.1.mDL",
  "session_transcript": "...",
  "verifier_pub": {
    "kty": "EC",
    "crv": "P-256",
    "x": "aPPK-...2-Drn",
    "y": "FqJqG...ksKHp"
  },
  "credential_response_encryption": {
    "jwk": {
      "kty": "EC",
      "crv": "P-256",
      "x": "1SptQyCUQiQD3dBcVHTclxRtcqhZlF1rIKcBR-i_WK4",
      "y": "8ayoQrh52zNuRQx8Q0gpmCpOjzbH397mOsaSi8X10c4"
    },
    "alg": "ECDH-ES",
    "enc": "A256GCM"
  }
}
```

`credential` parameter in the Credential Response MUST contain a
base64url-encoded `Document` which contains `IssuerSigned` and the
`DeviceSigned` parameters defined in section 8.3.2.1.2.2 of 18013-5.

The Device key included in the `IssuerSigned` and used to secure `DeviceSigned`
is generated by the PID Provider, and not by the Wallet as defined in ISO
18013-5.

SessionTranscript used by the PID Provider when calculating `DeviceSigned` is
calculated by the Wallet. Therefore, there is no need for the PID Provider to
know the specific values of `mdocGeneratedNonce`, `client_id`, `responseUri`,
and `nonce` parameters used for SessionTranscript calculation by the Wallet.
Moreover, there is also no way for the PID Provider to reconstruct these values,
as the SessionTranscript is hashed and contains random values chosen by the
wallet (i.e. `mdocGeneratedNonce`).

Below is a non-normative example of a Credential Response JWT (JWE) during the
PID issuance in mso Credential Format over Authenticated Channel:

```json
HTTP/1.1 200 OK
Content-Type: application/jwt
Cache-Control: no-store

{
  "epk": {
    "kty": "EC",
    "crv": "P-256",
    "x": "VJUVl-ZqLKzzncZ4Gs_nJfcqY_YBHPkGVN0sRSlF-3s",
    "y": "y4maAWKte676d0wL6um-8wAUJ9pW-mlc528BFeiXn2Y"
  },
  "enc": "A256GCM",
  "alg": "ECDH-ES"
}..
nLuW9PwrGl76pjTy.
nH_XE2LJ2u-N0o8M-tanqoYhJr7hbjo.
3uNrPqJazT9ZSf54sk1Ueg
```

The decrypted payload of the Credential Response JWT example results in the
following structure:

```json
{
  "credential": "..."
}
```

### OpenID4VCI Credential Request and Response for PID Issuance in SD-JWT Credential Format using Authenticated Channel

Note that this section is the same as in Option B.

The following parameter is defined in addition to those defined for the
Credential Format `vc+sd-jwt` in Annex A.3 of OpenId4VCI:

- `verifier_pub`: REQUIRED. A JSON object as defined in Section 2 of RFC7591. It
  contains the ephemeral RP key *rp_eph_pub* from the RP's Authorization
  Request.

New Credential Format identifier is not defined PID Issuance in SD-JWT
Credential Format using Authenticated Channel, because the issued Credential in
the Credential Response is SD-JWT, which is the same as the one defined in Annex
A.3 of OpenId4VCI. Since during PID Issuance in SD-JWT Credential Format using
Authenticated Channel, not just `IssuerSigned`, but also `DeviceSigned` is
returned in the Credential Response as part of the `Document` structure as
defined above, it is a significant deviation that necessitated the definition of
a new Credential Format identifier `mso_mdoc_authenticated_channel`.

`credential_response_encryption` parameter MUST be present. Note: The wallet can
find out about the supported cryptographic algorithms for the credential
response encryption by using the Issuer's metadata parameter
`credential_response_encryption`.

Below is a non-normative example of a Credential Request during the PID issuance
in IETF SD-JWT VC Credential Format over Authenticated Channel: Note: The wallet
can find out about the supported cryptographic algorithms by using the Issuer's
metadata parameter `credential_signing_alg_values_supported`.

```json
POST /credential HTTP/1.1
Host: server.example.com
Content-Type: application/json
Authorization: DPoP czZCaGRSa3F0MzpnWDFmQmF0M2JW
DPoP: ey…

{
  "format": "vc+sd-jwt",
  "vct": "SD_JWT_VC_example",
  "proof": {
    "proof_type": "jwt",
    "jwt": "eyJ0e...h1WlA"
  },
  "verifier_pub": {
    "kty": "EC",
    "crv": "P-256",
    "x": "aPPK-...2-Drn",
    "y": "FqJqG...ksKHp"
  },
  "credential_response_encryption": {
    "jwk": {
      "kty": "EC",
      "crv": "P-256",
      "x": "1SptQyCUQiQD3dBcVHTclxRtcqhZlF1rIKcBR-i_WK4",
      "y": "8ayoQrh52zNuRQx8Q0gpmCpOjzbH397mOsaSi8X10c4"
    },
    "alg": "ECDH-ES",
    "enc": "A256GCM"
  }
}
```

## Implementation Considerations

- When the issuance flow is specific to the PID Provider, alternatives to
  Presentation Exchange may be considered. If the issuance flow is to be
  generic, sticking to PE might be the best option.
- The JWA to use the x5c in the SD-JWT and perform ECDH key agreement and derive
  the MAC key is to be defined, a proposal is made
  [here](https://github.com/paulbastian/jose-ecdh-mac-algorithms).

## Usability Considerations

- Relying Party should inform users in advance of what is required for the
  process to be completed successfully and what steps follow
- It must be clarified whether the wallet app needs to be unlocked in this flow,
  as no sensitive data is displayed and the user must identify themselves with
  their ID card and eID PIN in order to successfully complete the process
- For reasons of transparency and to increase trust, Relying Parties should
  provide sufficient information (metadata) for the consent screen. This allows
  users to learn everything relevant e.g. about the relying party itself,
  privacy and data retention
- eID process is integrated in Wallet. No context switch to the AusweisApp is
  required
- Physical ID card is required for every transaction
- Online-Ausweisfunktion must be activated
- eID PIN must be set by the user (replacement of the Transport PIN) and be
  known to them so that they can successfully confirm the process
- Only the requested data attributes can be displayed on the consent screen, as
  the user's data values are not available at this point (only after a
  successful eID process)
- It must be ensured that users return to the correct tab in the correct browser
  in order to continue the process or know how to get there manually if
  necessary (especially for iOS devices, if the process was not started in the
  default browser)
- To eliminate the PID provider observing which attributes are presented, the
  PID provider will generate a PID with all attributes; then the wallet will
  generate the KB-JWT payload based on the Relying Party's request. While this
  approach increases privacy, it will decrease the user experience by confusing
  the user. According to the German eID card law, the client needs to display
  which attributes are read from the eID card. This means, for example, in case
  of a use case where only the age has to be verified, the client would have to
  display all PID attributes. This could scare the user and lead them to abort
  the process, or the information which are shared with the Relying Party are
  ignored by the user because there are two screens. This problem could be
  eliminated by introducing an exception for EUDI Wallet in the German eID law.

## Security Considerations

- In Step 8, a malicious app may spoof the wallet app by registering to the same
  custom URL scheme; an attacker may
    - **on the attacker's own device:** Capture the request and replay it to a
    victim on another device, thus having the victim identify itself in a
    context of the attacker (**Relaying Attack breaking Identification
    Context**); or
    - **on the victim's device:** Capture the request and spoof the whole
    identification process or parts of it (**Wallet App Spoofing**).
- In Step 10: An attacker acting as an RP can forward a request from a different
  RP.
    - As long as the request remains unchanged, we're in the **Relaying Attack
    breaking Identification Context**
    - If the attacker changes anything in the request, this will break the
    signature. The attacker could otherwise attempt to
        - insert the attacker's own ephemeral key, leading to an SD-JWT artifact
      that could be used in a different flow between the attacker and some other
      RP.
        - modify state or nonce or other data. **Q:** Any useful attacks
      resulting from this?
- In Step 14: Shall the Wallet Attestation be used at the Authorization Request?
  Which nonce to use for Wallet Attestation at the Token Request?
- In Step 15 and onward: Security of PID Provider Interface:
    - Are additional steps required to protect the interface to the PID
      Provider?
    - What exactly is the transaction binding?
    - How is the eID process tied to the process at the PID provider?
- In Step 37: The RP must not consider the user identified at this point; it is
  important to have the browser redirect in the later steps.

## Privacy Considerations

- For the user consent, the user should be informed in a meaningful way about
  the process for which the identification is performed. This includes the RP's
  domain name or assured legal name (via a certificate), the purpose of the
  identification and the context. Note: This may have privacy implications that
  need to be evaluated.
    - The Wallet must ensure that the purpose of the Relying Party is not
    transmitted to the PID Provider.
- Selective Disclosure is achieved by ensuring that the credential contains only
  those data items (claims) that are requested by the RP. The list of requested
  claims is part of the request to the PID Provider.
- An initiating step before the authorization request (Step 10) that is
  performed without the user's interaction might leak information about the
  user's device to the RP (**Initial Request Privacy Leak**). For example:
    - the fact that the wallet app is installed,
    - the fact which wallet app is installed,
    - some metadata about the wallet app contained in request headers (e.g.,
    version, language, etc.),
    - network information if the browser uses a VPN but the wallet app does not.
- The SessionTranscript is used to bind the authentication to a specific
  session:
    - The SessionTranscript structure for OpenID4VP contains a OID4VPHandover
    structure containing:
        - clientIDHash containing the SHA256 of the Relying Parties client_id
      and the mdocGeneratedNonce
        - responseUriHash containing the SHA256 of the Relying Parties
      responseUri and the mdocGeneratedNonce
        - nonce chosen by the Relying Party
    - As the relevant information is hashed, the Relying Party identity is not
    revealed to the PID Provider
- Unlinkability: Each Relying Party sees a new credential for each use of the
  flow. Unless the eID data itself enables linkability, there is no information
  enabling linkablility between different uses of the flow.
