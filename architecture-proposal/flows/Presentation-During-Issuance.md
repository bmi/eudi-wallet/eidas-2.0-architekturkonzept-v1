<!-- PDF-show
\newpage
-->

# Presentation during Issuance

The PID/(Q)EAA Provider might want to request presentation of the credential(s)
as a means to authenticate the user during the issuance of another credential.
This document extends a [dynamic credential
request](https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0.html#name-dynamic-credential-request)
in OID4VCI using the [OAuth 2.0 for First-Party
Applications](https://www.ietf.org/archive/id/draft-parecki-oauth-first-party-apps-02.html).
By doing so, it allows to start an OID4VP flow using the OID4VCI authorization
request during the authorization code flow and to improve the user experience
with a browser-less approach.

Below is a non-normative diagram illustrating the flow (it includes only the
components relevant to the illustration of the flow described in this document):

<!-- PDF-show
\newpage
-->

```plantuml

@startuml

'Ensure messages are not too wide
skinparam maxMessageSize 200
skinparam wrapWidth 300

'Macro for colored [TLS] block
!function tls()
!return "<color #118888>[TLS]</color>"
!endfunction

'Align text on arrows to center
skinparam sequenceMessageAlign center

'padding between boxes
skinparam BoxPadding 100

autonumber "<b>(000)"

title Presentation during issuance

participant w [
                Wallet
            ]
participant ci [
                Credential Issuer
            ]
participant as [
                Authorization Server
            ]
participant ov  [
                OID4VP Verifier                
            ]

w -> ci: select credential to receive
ci -> w: credential offer\n<Grant-Type: Authorization Code>
w -> ci: tls() HTTP GET ./well-known Credential Issuer Metadata
ci -> w: tls() HTTP 200 <Credential Issuer Metadata>

alt optional wallet attestation
  note over w, ci: Wallet attestation, see [[../EAA-Issuance-OpenID4VC#sequence-diagram]]
end

w -> as: tls() HTTP POST<AuthorizationChallengeRequest>

note over as,ov : generate OID4VP authorization request

group OID4VP flow
  as -> w: tls() HTTP 400 <AuthorizationErrorResponse(rp_authorization_request)>
  w -> ov: tls() HTTP POST <AuthorizationResponse(VP token with Verifiable Presentation)>
  ov -> w: tls() HTTP 200 <ValidationResponse(presentation_during_issuance_session)>
else credentials not present
  w -> ov: tls() HTTP POST <ErrorResponse(auth_session, error)>
  w -> as: tls() HTTP POST <AuthorizationChallengeRequest(auth_session)>
  note over as,ov: Authorization Server get's information about the session from the OID4VP Verifier. It can decide how to proceed.
end

w -> as: tls() HTTP POST <AuthorizationChallengeRequest(auth_session, presentation_during_issuance_session)>
as -> w: tls() HTTP 200 <AuthorizationCodeResponse>
w -> as: tls() HTTP POST <TokenRequest(authorization_code, wallet_attestation)>
as -> w: tls() HTTP 200 <TokenResponse>
w -> ci: tls() HTTP POST <CredentialRequest>
ci -> w: tls() HTTP 200 <CredentialResponse>

@enduml
```

<!-- PDF-show
\newpage
-->

## Step-by-Step Description

1. The Wallet selects the credential it wants to receive.
2. Optionally, the Credential Issuer sends a credential offer to the Wallet.
3. The Wallet obtains the Credential Issuer's metadata.
4. The Credential Issuers responds with the metadata.
5. The Wallet sends the [Authorization Challenge
   Request](https://www.ietf.org/archive/id/draft-parecki-oauth-first-party-apps-02.html#name-authorization-challenge-req)
   to the Authorization Challenge Endpoint of the Issuer's Authorization Server
   using HTTP POST.
   - See [Appendix](#example-authorization-challenge-request) for an example
     request.
   - In case a wallet attestation is required by the Issuer, it has to be
     included in this request. The Issuer has to track in the session that the
     wallet attestation was provided.
6. The Issuer determines whether the information provided to the Authorization
   Challenge Endpoint is sufficient to grant authorization for the credential
   issuance. If the presentation of other credential(s) is required, the Issuer
   generates presentation request for those credential(s), and sends it to the
   Wallet in the Authorization Challenge Error Response. It uses
   [presentation](#presentation) parameter defined in this document to do so.
    - The Issuer is acting as a Relying Party to the Wallet when it is
      requesting credential presentation. The exact architecture and the
      deployment of the Issuer's Authorization Server in OID4VCI and RP in
      OID4VP is out of scope of this flow: they can be different services or the
      same service.
    - The [auth_session](#auth_session) value from the OID4VCI flow can be used
      by the Issuer for state management between issuance and presentation
      flows.
    - The RP will start the OID4VP flow with the steps from [EAA Presentation
      OpenID4VC](./EAA-Presentation-OpenID4VC.md), with the steps 4 to 5.
    - An example Authorization Challenge Response is in the
      [Appendix](#example-authorization-error-response).
7. In case the Wallet is able to present the required credential, it will send
   and authorization request to the RP.
8. In case the Wallet is not able to present the credentials
    - The Wallet will respond with an [Error response](#example-error-response),
      including the [auth_session](#auth_session) value and the error code to
      the RP.
9. The Wallet will send another authorization challenge including the
   [auth_session](#auth_session) value to the Issuer's Authorization Server like
   in step 4.
    - The Issuer has to contact the RP to get information about the state of the
      session and to decide how to proceed (creating a new presentation request,
      send a redirect to the browser, etc.).
10. If the RP has successfully processed the Authorization Response or
    Authorization Error Response, it responds with a status code 200 as defined
    in OID4VP. When doing so, the RP MUST send information about the session in
    `presentation_during_issuance_session` parameter in the body of the
    response.
    - The content of the
      [presentation_during_issuance_session](#presentation_during_issuance_session)
      parameter is out of scope of this document. It could be the Verifiable
      Presentation itself represented as a JWT, or a state or nonce from the
      presentation session.
    - The content of this parameter is opaque to the wallet.
    - See [Appendix](#example-validation-response) for an example response.
11. The Wallet sends the request to the Authorization Challenge Endpoint of the
    Issuer by issuing a POST request via the Wallet with the
    [auth_session](#auth_session).
    - See [Appendix](#example-authorization-challenge-request-with-session) for
      an example request.
    - The Wallet will use the value from the response and pass it in the
      [presentation_during_issuance_session](#presentation_during_issuance_session)
      parameter.
12. The Issuer validates the request, and in case of a successful validation
    response with an [authorization
    code](https://www.ietf.org/archive/id/draft-parecki-oauth-first-party-apps-02.html#name-authorization-code-response).
13. The Wallet makes a request to the Token Endpoint of the Issuer to exchange
    the authorization code for an access token.
14. The Authorization Server responds with a [Token
    Response](https://www.ietf.org/archive/id/draft-parecki-oauth-first-party-apps-02.html#name-token-endpoint-successful-r).
    - An example response is in the [Appendix](#example-token-response).
15. The Wallet sends the Credential Request to the Issuer.
16. The Issuer sends the Credential Response to the Wallet.

## Known limitations

This flow is based on the "OAuth 2.0 for First-Party Applications" draft, and
aims for a browser-less approach to improve user experience. This comes with the
limitation that the credential presentation request is handled by the same
Wallet that was used to start the OID4VCI process. In case not all of the
requested credentials are available in this wallet, the wallet either needs to
respond with an error, or to have a way to request issuance of the missing
credential(s).

It is recommended to indicate to the issuer that requested credential(s) are not
available in the wallet in the subsequent authorization challenge request.

## Security Considerations

Please have a look at the [Security
Considerations](https://www.ietf.org/archive/id/draft-parecki-oauth-first-party-apps-02.html#name-security-considerations)
for the first party application usage, especially for the [auth session
binding](https://www.ietf.org/archive/id/draft-parecki-oauth-first-party-apps-02.html#name-auth-session-dpop-binding).

## Changes to the OID4VC Flow

With the usage of the first party application flow, the issuer metadata need to
be extended with the
[authorization_challenge_endpoint](https://www.ietf.org/archive/id/draft-parecki-oauth-first-party-apps-02.html#name-authorization-server-metada).

Since the OID4VCI flow includes an authorization flow, the lifetime of the
session has to be increased.

### Additional parameters

The following parameters are defined in this flow and are not part of the normal
OID4VCI or first party app specifications:

#### auth_session

REQUIRED. The auth session allows the authorization server to associate
subsequent requests by this client with an ongoing authorization request
sequence. The client MUST include the auth_session in follow-up requests to the
authorization challenge endpoint if it receives one along with the error
response.

#### presentation

REQUIRED. String containing the OID4VP request URI. The Wallet will use this URI
to start the OID4VP flow.

#### presentation_during_issuance_session

OPTIONAL. String containing information about the session when credential
presentation is happening during issuance of another credential. The content of
this parameter is opaque to the wallet. When this parameter is present the
Wallet MUST use this parameter in the subsequent Authorization Challenge
Request. This allows the Issuer to determine which it can be used by to prevent
session fixation attacks. The Response URI MAY return this parameter in response
to successful Authorization Responses or for Error Responses.

## Appendix: Examples

### Example Authorization Challenge Request

Extend the request with the [authorization details
parameter](https://openid.github.io/OpenID4VCI/openid-4-verifiable-credential-issuance-wg-draft.html#name-using-authorization-details)
or the
[scope](https://openid.github.io/OpenID4VCI/openid-4-verifiable-credential-issuance-wg-draft.html#name-using-scope-parameter-to-re).
The `request_uri`, `code_challenge`, `code_challenge_method`, `response_type`
authorization request parameters MUST NOT be provided.

```http
POST /authorize-challenge HTTP/1.1
Host: server.example.com
Content-Type: application/x-www-form-urlencoded

client_id=bb16c14c73415
```

### Example Authorization Error Response

```http
HTTP/1.1 400 OK
Content-Type: application/json
Cache-Control: no-store

{
  "error": "insufficient_authorization",
  "auth_session": "123456789",
  "presentation": "/authorize?client_id=..&request_uri=https://rp.example.com/oidc/request/1234"
}
```

### Example Validation Response

```http
HTTP/1.1 200 OK
Content-Type: application/json
Cache-Control: no-store

{
  "presentation_during_issuance_session": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX..."
}
```

### Example Error Response

Sending a request according to the
[spec](https://openid.net/specs/openid-4-verifiable-presentations-1_0.html#section-6.4-8)

```http
POST /authorize-challenge HTTP/1.1
Host: server.example.com
Content-Type: application/x-www-form-urlencoded
error=access_denied&auth_session=123456789
```

### Example Authorization Challenge Request with session

```http
POST /authorize-challenge HTTP/1.1
Host: server.example.com
Content-Type: application/x-www-form-urlencoded

client_id=bb16c14c73415&auth_session=123456789&presentation_during_issuance_session=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpX...
```

### Example Authorization Code Response

```http
HTTP/1.1 200 OK
Content-Type: application/json
Cache-Control: no-store

{
  "authorization_code": "uY29tL2F1dGhlbnRpY"
}
```

### Example Token Response

```http
HTTP/1.1 200 OK
Content-Type: application/json
Cache-Control: no-store

{
  "access_token": "2YotnFZFEjr1zCsicMWpAA",
  "token_type": "Bearer",
  "expires_in": 3600,
  "refresh_token": "tGzv3JOkF0XG5Qx2TlKWIA",
  "auth_session": "123456789"
}
```
