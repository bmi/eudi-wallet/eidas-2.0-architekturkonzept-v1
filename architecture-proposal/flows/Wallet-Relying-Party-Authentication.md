# Wallet-Relying Party Authentication

This document outlines a proposed solution for implementing the Wallet-Relying
Party (RP) authentication requirements from [IA
5b](https://ec.europa.eu/info/law/better-regulation/have-your-say/initiatives/14399-European-Digital-Identity-Wallets-registration-of-relying-parties_en).

## Motivation

Aligned with the eIDAS 2 regulation and the Implementing Acts, the proposed
solution enables straightforward onboarding of Relying Parties (RPs) and
provides users with automated validation of the intended use of their data by
the Wallet. Rather than relying on the Authorization Certificate
(Berechtigungszertifikat) used in Germany’s eID system for RP authentication and
authorization, this approach introduces a more flexible model.

The authentication requirements for RPs interacting with the European Digital
Identity Wallet (EUDI Wallet) are derived from Article 5b of the eIDAS 2
regulation. The following steps outline the process:

- **RP Registration**: RPs must register in their respective EU member state in
  both human- and machine-readable formats, providing:
    - Essential details (name, identifiers from other registers, registration
      number).
    - Specification of the data they intend to request from EUDI Wallets.
- **Authentication with the EUDI Wallet**: **Authentication with the EUDI
  Wallet**: Before interacting with the RP, EUDI Wallet needs to authenticate
  the RP. Authentication fails if the trust chain cannot be successfully
  verified and the EUDIW aborts the process.
- **Validation of Data Requests**: The EUDI Wallet ensures that RP presentation
  requests align with their declared intended use. Machine-readable formats play
  a central role in enabling automated validation while ensuring transparency
  for the users, consolidating both clarity and trust in data usage processes.
  EUDIW fails the process if the request exceeds the declared intended use and
  aborts the presentation process.

When the RP is using a hosted "Verifier as a Service" solution, presented
certificates (Access Certificate and Registration Certificates) have to belong
to the RP and not the one of the service provider. The RP has to make it clear
who is the provider it is using, for example, by mentioning it in its privacy
policy.

## Trust Architecture

The proposed trust architecture involves a Trusted List, Wallet-Relying Party
Registrar (RPR), and Wallet-Relying Party (RP) based on the following
principles:

- **Direct Authentication**: RPs include certificates and attestations directly
   in their requests to the EUDI Wallet, supporting both online and offline
   interactions while reducing Wallet's communication with Registrars. This
   avoids potential tracking of Wallet's interactions by the Registrars.
- **Separation of Attestations**:
    - Identification data and intended use are managed as separate certificates
     (Access Certificate and Registration Certificate).
    - RPs can include only the attestations relevant to the requested data in
     each presentation request.
    - All attestations are publicly accessible via the registrar for review by
      the third parties.
- **Machine processable intended use**:
    - Wallets are able to match the intended use with the presentation request.
    - RPs are able to update their intended uses, but the request must never
        exceed the set of registered data.

```plantuml
digraph G {
    rankdir=LR
    node [shape=box]    
    
    // Define ranks
    { rank=same; tl; wrp_registrar }
    { rank=same; ac; rc; wallet }
    { rank=same; vp; wrp }

    // Nodes
    tl [label="Trusted List"]
    wrp [label="Wallet-Relying Party"]
    wrp_registrar [label="Wallet-Relying Party Registrar"]
        
    wallet [label="Wallet"]
    ac [label="Access Certificate", fillcolor=yellow, style=filled]
    rc [label="Registration Certificate", fillcolor=yellow, style=filled]
    vp [label="Verifiable Presentation"]

    // Edges
    wrp -> wrp_registrar [label="onboarded"]
    wrp_registrar -> tl [label="published"]
    wrp_registrar -> ac [label="issues"]
    wrp_registrar -> rc [label="issues"]
    ac -> wrp [label="issued for"]
    rc -> wrp [label="issued for"]
    vp -> wrp [label="presented to"]
    wallet -> vp [label="creates"]
    ac -> vp [label="used for authentication"]
    rc -> vp [label="used for intended use"]

    // Invisible edges to maintain order
    tl -> wrp_registrar [style=invis]
    wrp_registrar -> ac [style=invis]
    ac -> vp [style=invis]
}
```

## International interoperability

The format of the following certificates and how to include them in the
presentation request need to be aligned across the EU to ensure interoperability
among the Wallets and the RPs:

- Access Certificate
- Registration Certificate

The following can be specified individually by each member state without
impacting interoperability:

- RP Registrar onboarding
- Interaction with the RP Registrar

## Out of Scope

The onboarding of a natural and legal persons of each kind are out of scope for
this document. For interoperability between the member states, the onboarding to
the registrars can be unique and must not align with each other, but the access
certificates and intended use proofs have to.

## Registrar

### Registration

To interact with an EUDI Wallet in any role (issuer, verifier, etc.), the RP has
to register at the RPR to receive the required certificates. The exact process
of the onboarding of a RP is not defined yet since there are multiple ways that
need to be defined by a policy first.

### Authentication management

To request data or receive credentials, the RP must first authenticate with the
EUDI Wallet so the user knows with whom they are interacting.

The process of receiving an [Access Certificate](#access-certificate) can be
automated on the RPR side since the identification of the RP is done during the
onboarding process. This allows the RP without any delay to receive new
certificates for key rotation or adding new services.

The revocation of an access certificate by the RP can be done any time without a
delay to reduce the risk of a data breach. The revocation of an access
certificate can not be undone.

```plantuml
@startuml

'Ensure messages are not too wide
skinparam maxMessageSize 200
skinparam wrapWidth 300

'Align text on arrows to center
skinparam sequenceMessageAlign center

'padding between boxes
skinparam BoxPadding 100

autonumber "<b>(000)"

title Access Certificate request

participant rp order 1 [
    Wallet Relying Party
]

participant rpr order 2 [
    Wallet Relying Party Registrar
]

rp -> rpr: HTTP <AccessCertificateRequest>
rpr -> rpr: create and publish Access Certificate
rpr -> rp: HTTP 200 Access Certificate

@enduml
```

Step by Step:

1. RP sends request to the RPR
    - request includes only the public key of the RP
    - includes authentication data like bearer token from previous login step
2. RPR evaluates the request, creates an [Access
   Certificate](#access-certificate) and publishes it
    - a status management is added
    - identity data from from the internal database is added
3. RPR sends the Access Certificate to the RP
    - RP can use it in all requests to the Wallet

### Intended use management

When an RP intends to request user data, the intended use must be registered in
advance. This intended use is structured according to a defined schema,
utilizing the [Dynamic Credential Query
Language](https://openid.net/specs/openid-4-verifiable-presentations-1_0.html#name-digital-credentials-query-l),
enabling the Wallet to process the request. The RPR does not verify whether the
RP is authorized to request the data. The concept of specifying the intended use
for processing personal data is comparable to the existing privacy policies
commonly in use today.

```plantuml
@startuml

'Ensure messages are not too wide
skinparam maxMessageSize 200
skinparam wrapWidth 300

'Align text on arrows to center
skinparam sequenceMessageAlign center

'padding between boxes
skinparam BoxPadding 100

autonumber "<b>(000)"

title Registration Certificate request

participant rp order 1 [
    Wallet Relying Party
]

participant rpr order 2 [
    Wallet Relying Party Registrar
]

rp -> rp: create payload of the Registration Certificate
rp -> rpr: HTTP <IntendedUseRegistration>
rpr -> rpr: sign and publish Registration Certificate
rpr -> rp: HTTP 200 Registration Certificate

@enduml
```

Step by Step:

1. RP creates an intended use
2. RP sends request to the RPR
3. RPR evaluates the request, creates a [Registration
   Certificate](#registration-certificate) and publishes it
    - a status management is added
    - the subject is linked to the RP Access Certificate distinguished name
4. RPR sends the Registration Certificate to the RP
    - it will allow the RP to attach it in a presentation request without
      pointing to an endpoint, risking privacy (RPR would see which Wallet is
      requesting which RP's intended use)

## Access Certificate

The RP Access Certificate is a x509 certificate based on the [RFC
5280](https://datatracker.ietf.org/doc/html/rfc5280#section-4.1.1), including
the relevant attributes to identity a RP. It must just include the values for
authentication, not the entitlements or the intended use.

A RP is able to request multiple Access Certificates from the RPR and use them
in parallel. A RP can request the revocation of any of its certificates at any
time through the RPR's API. While this approach increases the complexity for the
RPR, it simplifies processes on the EUDI Wallet side, as the validation of the
trust chain and the mapping of the intended use remain consistent. It also helps
with the compliance since the Access Certificates need to be available via the
registrar for ten years, even after the RP shut down its business.

**Subject**: the distinguished name (DN) of the RP, including the following
fields. In case the RP is a legal person, the `CN` should be the name of the
legal person and the `O` field needs to be present. In case the RP is a natural
person, the `CN` should be the name of the natural person and the `GN`and `SN`
should be present. The list of fields only includes the already defined fields
that have an OID.

| **Attribute**            | **Abbreviation** | **Description**                                 | **Example Value**              |
|---------------------------|-----------------|-------------------------------------------------|--------------------------------|
| **Common Name**           | `CN`            | Identifies the entity (e.g., person, company).  | `SPRIND GmbH`, `Erika Mustermann`|
| **Organization**          | `O`             | The name of the organization.                   | `SPRIND GmbH`                  |
| **Country**               | `C`             | The ISO 3166-1 two-letter country code.         | `DE`                           |
| **Email Address**         | `E`             | Email address of the entity (non-standard).     | `info@sprind.org`              |
| **Given Name**            | `GN`            | First name of the individual (optional).        | `Erika`                        |
| **Surname**               | `SN`            | Last name or family name (optional).            | `Mustermann`                   |

**Issuer**: the distinguished name of the RP Registrar, including the following
fields:

| **Attribute**            | **Abbreviation** | **Description**                                 | **Example Value**              |
|---------------------------|-----------------|-------------------------------------------------|--------------------------------|
| **Common Name**           | `CN`            | Identifies the registrar                        | `bundesnetzagentur`            |
| **Country**               | `C`             | The ISO 3166-1 two-letter country code.         | `DE`                           |

**Certificate Policies**: the RP access certificate must include the policy of
the RP Registrar.

**Validity**: the `notBefore` and `notAfter` fields define the certificate’s
validity period.

**Status Management**: the RP Registrar must maintain a certificate revocation
list to revoke the RP access certificate when necessary. The revocation list
should be accessible via a URL. To protect the users privacy, OCSP should not be
used. The privacy preserving approach with OCSP stapling can not be used since
it was designed to be used with TLS, but the transport protocols of the EUDI
Wallet do not all depend on TLS.

**Key Usage**: to be discussed if required.

**Official identifiers**: where applicable, the RP's identifiers from official
records should be included like

- an Economic Operators Registration and Identification (EORI) number as
  referred to in Commission Implementing Regulation (EU) No 1352/2013 ;
- the registration number as registered in a national business register;
- a Legal Entity Identifier (LEI) as referred to in Commission Implementing
  Regulation (EU) No 2022/1860 ;
- a VAT registration number;
- an excise number as specified in Article 2(12) of Council Regulation (EC) No
  389/2012 ;
- a tax reference number;
- a European Unique Identifier (EUID) as referred to in Commission Implementing
  Regulation (EU) 2020/2244.

The semantic of the table below is based on [ETSI EN 319 412-1 V1.5.1](https://www.etsi.org/deliver/etsi_en/319400_319499/31941201/01.05.01_60/en_31941201v010501p.pdf)

| **Attribute**                                         | **Abbreviation**          | **Description**                                      | **Format**                          | **Example Value**            |
|------------------------------------------------------|--------------------------|----------------------------------------------------|------------------------------------|------------------------------|
| **Economic Operators Registration and Identification (EORI) Number** | `organizationIdentifier` | Unique identifier for economic operators in the EU. | `EORI<CountryCode>-<Identifier>`   | `EORIDE-1234567890`          |
| **National Business Register Number**               | `organizationIdentifier` | Identifier from a national trade register.         | `NTR<CountryCode>-<Identifier>`    | `NTRDE-HRB123456`            |
| **Legal Entity Identifier (LEI)**                   | `organizationIdentifier` | Global identifier based on ISO 17442.              | `LEIXG-<Identifier>`               | `LEIXG-529900T8BM49AURSDO55` |
| **VAT Registration Number**                         | `organizationIdentifier` | Identification based on a national VAT number.     | `VAT<CountryCode>-<Identifier>`    | `VATDE-123456789`            |
| **Excise Number**                                   | `organizationIdentifier` | Identifier for excise duty purposes.               | `<CountryCode>:EX-<Identifier>`    | `DE:EX-987654321`            |
| **Tax Reference Number**                            | `organizationIdentifier` | National tax reference number.                     | `<CountryCode>:TX-<Identifier>`    | `DE:TX-123456789`            |
| **European Unique Identifier (EUID)**               | `organizationIdentifier` | Business register number compliant with EUID format. | `EI<CountryCode>-<Identifier>`     | `EI:SE-5567971433`           |

## Registration Certificate

The registration certificate is a JWS or COSE_Signature that includes the
intended use of the RP. The JWS/COSE_SIGNATURE is signed by the RP Registrar and
includes the following fields:

**`purpose`**:

- An object [supporting multiple languages](#multi-language-support-for-purpose)
  on the root level that describes the purpose for which the data is requested.
- This field allows the user and external parties to evaluate whether the
  intended use aligns with the requested data.
- Unlike the purpose of a VP (Verifiable Presentation) request, which is unique
  and unverified, the `purpose` can be audited and reviewed externally
  beforehand, providing greater transparency.
- This information is presented to the user instead of the VP request’s purpose
  for clarity.

**`credentials` and `credentialsSets`**:

- These are structured according to the [DCQL
  specification](https://openid.net/specs/openid-4-verifiable-presentations-1_0.html#name-digital-credentials-query-l),
  ensuring compatibility with established standards for querying and presenting
  credentials.
- The Wallet will not check if the `purpose` field and the `credentials` field
  are matching. It will check if the `credentials` field, that is a list, of the
  VP request is equal or a subset of the `credentials` field of the passed
  Registration Certificate.

**Extension of the Claim Object**:

- A RP can define on `claim` level the `purpose` of each attribute it want's to
  request. Since most credential formats are supporting selective disclosures,
  the RP has to explain why a field needs to be requested.
- A `purpose` field may be added to the credentials query allowing a more
  granular explanation than the broader `purpose` field in the credential set
  query, enhancing privacy.
- In case a `purpose` field is defined in the Registration Certificate and the
  attribute is requested by the RP, the Wallet should present this purpose value
  instead of the one that got passed in the presentation request. This
  guarantees that this purpose definition that is presented to the user is equal
  to the one that is public available in the registry.
- In contrast to the DCQL, all values in the credential set are mandatory, and
  optional flags are not permitted. In terms of the privacy policy, the RP needs
  to define the purpose independent if the attribute is optional or not.

**`contact`**:

- Contact details specific for the intended use are included in the JWS, as they
  are not part of the authentication certificate. It is up to the registrar to
  verify the contact information during the creation process of the Registration
  Certificate.

**`sub`**:

- The `sub` field is required and should include the Distinguished Name (DN) of
  the RP Access Certificate.

**`privacy_policy`**:

- A URL linking to the RP’s privacy policy, providing users with additional
  information about data processing and storage.

**`iat`**:

- Defines the time the Registration Certificate was issued.

**`exp`**:

- Optional and can be used to define an expiration date for the certificate.

**`purpose`**:

- The purpose field is a text-based description of the intended use of the data,
  see [multi language support](#multi-language-support-for-purpose) chapter for
  more information.

**`status`**:

- A status management system is required for the RP Registrar to revoke an
  attestation when necessary.
- Revocation is performed by the RP Registrar. It may also be triggered by the
  RP’s request or by a third party if the Registration Certificate becomes
  invalid.

This structure supports granular, auditable, and privacy-preserving handling of
credentials and intended use, aligned with DCQL standards and practical
requirements for transparency and governance.

```json
{
    "x5c": [] //includes  RPR certificate which the whole trust chain up to the EU list
}
.
{  
  "purpose": [
    {
      "locale": "en-US",
      "name": "Onboarding of a new user to the system."
    }
  ],
  "contact": {
    "address": "Lagerhofstr. 4, 04103 Leipzig, Germany",
    "email": "privacy@sprind.org",
    "phone": 1234566789,
  },
  "sub": "CN=bundesnetzagentur, C=DE",
  "privacy_policy": "https://sprind.org/en/privacy-policy",
  "iat": 1683000000,
  "credentials":[
    {
      "id":"pid",
      "format":"vc+sd-jwt",
      "meta":{
        "vct_values":[
          "https://credentials.example.com/identity_credential"
        ]
      },
      "claims":[
        {
          "path":[
            "given_name"
          ],          
          "purpose": [
            {
              "locale": "en-US",
              "name": "required for identification"
            }
          ],
        },
        {
          "path":[
            "family_name"
          ],
          "purpose": [
            {
              "locale": "en-US",
              "name": "required for identification"
            }
          ],
        },
        {
          "path":[
            "address",
            "street_address"
          ],
          "purpose": [
            {
              "locale": "en-US",
              "name": "required to send mails"
            }
          ],
        }
      ]
    },
    {
      "id":"other_pid",
      "format":"vc+sd-jwt",
      "meta":{
        "vct_values":[
          "https://othercredentials.example/pid"
        ]
      },      
      "purpose": [
        {
          "locale": "en-US",
          "name": "identification of a person"
        }
      ],        
      "claims":[
        {
          "path":[
            "given_name"
          ]
        },
        {
          "path":[
            "family_name"
          ]
        },
        {
          "path":[
            "address",
            "street_address"
          ]
        }
      ]
    },
    {
      "id":"pid_reduced_cred_1",
      "format":"vc+sd-jwt",
      "meta":{
        "vct_values":[
          "https://credentials.example.com/reduced_identity_credential"
        ]
      },
      "claims":[
        {
          "path":[
            "family_name"
          ]
        },
        {
          "path":[
            "given_name"
          ]
        }
      ]
    },
    {
      "id":"pid_reduced_cred_2",
      "format":"vc+sd-jwt",
      "meta":{
        "vct_values":[
          "https://cred.example/residence_credential"
        ]
      },
      "claims":[
        {
          "path":[
            "postal_code"
          ]
        },
        {
          "path":[
            "locality"
          ]
        },
        {
          "path":[
            "region"
          ]
        }
      ]
    }
  ],
  "credential_sets":[
    {      
      "purpose": [
        {
          "locale": "en-US",
          "name": "Identification"
        }
      ],           
      "options":[
        [
          "pid"
        ],
        [
          "other_pid"
        ],
        [
          "pid_reduced_cred_1",
          "pid_reduced_cred_2"
        ]
      ]
    }
  ],  
  "status": {
    "status_list": {
      "idx": 0,
      "uri": "https://example.com/statuslists/1"
    }
  }  
}
```

TODO: add an example for a COSE Signature.

### Multi language support for purpose

To support multiple languages, the `purpose` field in registration certificate
and the authorization certificate must be a list of objects. At least the one
for English with `en-US` has to be defined. Based on the user preferences, the
correct element should be used to display the purpose. Is the language the user
configured is not in the list, it's up to the Wallet if the English entry should
be used.

```json
[
    {
        "locale": "en-US",
        "name": "Required for checking the minimum age",
    },
    {
        "locale": "de-DE",
        "name": "Benötigt für die Überprüfung des Mindestalters"
    }
]
```

- `locale`: REQUIRED: String value that identifies the language of this object
  represented as a language tag taken from values defined in BCP47
  [RFC5646](https://datatracker.ietf.org/doc/html/rfc5646). Multiple display
  objects MAY be included for separate languages. There MUST be only one object
  for each language identifier.
- `name`: REQUIRED: String value that defines the purpose.

## Registrar Requirements

While the Implementing Acts do not require RP registrars to have standardized
APIs, the following set of Relying Party Registrar Requirements would benefit
all participants by ensuring consistent access to information.

- Reading information from the RP Registrar should be free of charge.
- Security measures like rate limiting should be implemented to prevent attacks.
- An OpenAPI-based API and a website with a question form should be available
  for reading information from the registrar.
- Users must be able to retrieve RP details based on the name or other
  identifiers.
- Revoked certificates, whether they are access or registration certificates,
  must be included in the response from the Registrar.
    - Clear guidelines must define situations where invalid content needs to be
    removed. For example, certificates containing personal data can be
    considered invalid. They should be entirely removed and not just be marked
    as deleted.
- Each certificate entry must include the issuance, expiration, and revocation
  timestamps in the response.
- Authentication to the Registrar should follow the OAuth 2.0 standard.
