<!-- PDF-show
\newpage
-->

# Trust Management and Infrastructure

## RP Trust Management

This section is based on the following assumptions:

* The Relying Party must register in order to be able to request credentials
  from EUDI Wallets and the authorization to request credentials from EUDI
  Wallets can be suspended or revoked.
* The authentication mechanism must allow a user to reliably identify and
  authenticate a Relying Party. This shall ensure the user knows what entity
  requests their data.

RPs are authenticated by the Wallet using x.509 certificates (with the details
of this check still TBD).

It is assumed there will be a registration mechanism where an RP (or the legal
representatives of that RP, respectively) can apply for the issuance of suitable
certificates. It is further assumed there will be different Certification
Authorities authorized to issue such certificates across the EU. Every EUDI
Wallet MUST be able to authenticate and authorize RPs registered in any of the
EU member states. Please note that the authorization at this point must not be
confused with the confirmation of the presentation of credentials by the user.
These are two different mechanisms. How the authentication and authorization
should be realized in detail is still under discussion.

The concrete mechanisms to register RPs is out of scope of this document.

The mechanisms to establish trust in the RP certificates are TBD.

Note: In addition, the Wallet instance should make all transactions (Issuance
and presentation processes for PID and (Q)EAAs) transparent to the user after
the fact, e.g., through a dashboard. Such information should be managed and
stored by the Wallet instance only.

## Wallet Trust Management

Within the eIDAS ecosystem, Wallets need to authenticate towards Issuers and
Relying Parties in order to prove their authenticity and trustworthiness.
Therefore, Wallet Providers or other parties authorized for that purpose by the
respective national competent authority issue assertions regarding particular
Wallet Instances. These assertions are referred to as "Wallet Attestations" or
"Wallet Trust Evidence" within this document. The Wallet Provider may set up a
pseudonymous user account for a particular user during the installation and
activation of the Wallet Instance and associate various Wallet Attestations with
this user account.

A Wallet Trust Evidence is signed by the Wallet Provider backend. These keys or
certificates will be published on a trust list or other trust mechanism, which
is TBD. By presenting the Wallet Trust Evidence during issuance process, the PID
or (Q)EAA Providers may validate the authenticity of the Wallet Instance and
verify that certain requirements for regulated use cases can be achieved. There
have been discussions on the European level whether the Relying Party must
directly verify the validity of the Wallet or if it can be implicitly done by
the attestation provider and the RP just trusts the validity of the attestation
(chain revocation). Currently, the legal interpretation of the regulation seems
to favor the former. For that, the "Wallet Instance Attestation", is sent to the
Relying Party during presentation. In contrast to the Wallet Trust Evidence,
which may give the PID / (Q)EAA Provider additional metadata, like certification
information of the WSCD, the Wallet Instance Attestation gives the Relying Party
only the information about the status of the Wallet Instance. The implementation
within this architecture proposal may change when consensus on the legal
interpretation changes.

For the [OpenID for Verifiable Credentials](https://openid.net/sg/openid4vc/)
protocol family, the Wallet Attestations are JWTs conforming to the definition
given in the [OpenID4VC High Assurance Interoperability Profile with SD-JWT
VC](https://openid.github.io/oid4vc-haip-sd-jwt-vc/openid4vc-high-assurance-interoperability-profile-sd-jwt-vc-wg-draft.html).
The Wallet Attestation mechanism ([OAuth 2.0 Attestation-Based Client
Authentication](https://datatracker.ietf.org/doc/draft-ietf-oauth-attestation-based-client-auth/))
is yet to be finalized, so the details are subject to change.

A more detailed, generic flow for the issuance of a Wallet Attestation to the
Wallet Instance is shown in the following linked description. Various issuance
flows described in this document will refer to that description.

[Description of the generic Wallet Attestation
flow](./flows/Wallet-Attestation.md)

For Relying Parties it would be helpful if all wallet implementations in Europe
are sharing the same invocation mechanism. To ensure that only valid Wallets can
be addressed we created the following
[proposal](appendices/EUDI_Link_Governance.md).

## Issuer Trust Management

The ARF defines a number of issuing entities as part of the EUDI Wallet
ecosystem, such as PID Providers, Qualified Trust Service Providers (QTSPs)
issuing QEAAs/QES and non-qualified Trust Service Providers (TSPs) issuing EAAs.
Each of these Issuers provide attestations according to their individual policy.
A policy for PID Providers is given by eIDAS and requires Level of Assurance
High. Policies for issuance of qualified certificates and the notification of
Authentic Sources for the issuance of EAAs are also known under eIDAS. Other
policies may have to be developed and are dependent on the respective business
cases.

According to the ARF, a trust relationship between the Relying Party and the
respective issuing entity must exist when releasing an attestation from the
Wallet to the Relying Party. In general, a Relying Party validates an
attestation by verifying an digital signature linked to the attestation and
created by the respective issuing entity under a certain Public Key
Infrastructure (PKI). It is assumed that the PKI and the respective certificate
of the issuing entity is linked to a policy under that the attestation and
digital signature was created. In order the get an attestation accepted by the
Relying Party, the Relying Party must accept the respective issuing policy.
