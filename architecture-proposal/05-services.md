<!-- PDF-show
\newpage
-->

# Additional Services

*NOTE: THIS PART WILL BE DEVELOPED IN A FUTURE RELEASE*

## Pseudonyms

TBD

## Transaction Authorization

TBD

### QES

TBD

### Payments

TBD

## Multilingual Support

The list of languages that, at the minimum, need to be supported by the participants in the EUDI ecosystem will be defined by BMI. Participants may choose to support other languages, too.

For interoperability, the following aspects of the protocols need to have multilingual support:

- Displaying of the Credential information based on the SD-JWT VC type metadata 
- Displaying of information about the Credentials and the Issuer based on the Credential Issuer metadata in OpenID4VCI
- Displaying the purpose of the transaction based on the Credential query in the Presentation Request.

This is a requirement mainly on app-level (not for the APIs).