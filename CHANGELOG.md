## [2.5.1](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/compare/v2.5.0...v2.5.1) (2025-03-10)


### Bug Fixes

* improve-documentation-on-dpop-nonce-fetching-for-c ([5f6e3d6](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/commit/5f6e3d65579653f0d825ae82a8ef4ee220782ad0))

# [2.5.0](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/compare/v2.4.6...v2.5.0) (2025-03-10)


### Features

* add relying party authorization ([92c2a8f](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/commit/92c2a8fe704e0d45b5f00c71141aab8571a09a1b))

## [2.4.6](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/compare/v2.4.5...v2.4.6) (2025-01-29)


### Bug Fixes

* correct statement about format examples ([ac80195](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/commit/ac801953cad264545ddb8649d9b286c619ef4d16))

## [2.4.5](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/compare/v2.4.4...v2.4.5) (2025-01-21)


### Bug Fixes

* add refresh token to c flow ([f9a0b0a](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/commit/f9a0b0a2758d77f4df5d4e7bd304d445ef570428))
* add section how to pass key attestation ([c259ced](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/commit/c259ced34542423cc72f87f89e53d7a4051a9d59))
* format file, extend credential profiles that can be used for (q)eaa ([4b618e0](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/commit/4b618e06cc1e9d67e07c1fa5a3e400d9496b7516))

## [2.4.4](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/compare/v2.4.3...v2.4.4) (2025-01-21)


### Bug Fixes

* execute page job at the end ([24e877c](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/commit/24e877c1b7ce36a33250ead9f71b72b3a1ea569c))

## [2.4.3](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/compare/v2.4.2...v2.4.3) (2025-01-16)


### Bug Fixes

* update readme for mkdocs ([86f3a20](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/commit/86f3a20d41780cf0d5aefe60391f90a0970e29a9))

## [2.4.2](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/compare/v2.4.1...v2.4.2) (2025-01-16)


### Bug Fixes

* add repo to new structure ([5bf9dc9](https://gitlab.opencode.de/bmi/eudi-wallet/eidas-2.0-architekturkonzept/commit/5bf9dc9f35654fe06d273803968a5e9ec2c9e450))

### Version 2.4 (October 2024)

Major changes:

* Presentation during issuance flow

Other changes:

* Refactored C', renamed keys, cleaned up the dependencies diagram, and removed
  text duplication with modularization to improve maintainability and clarity.

### Version 2.3 (August 2024)

Major changes:

* Add list of used standards and their versions
* Add rendered HTML version of the architecture document
* add information for the batch size
* update ECDH and MAC draft url
* add clarification for issuer MACing device signed in B and B'
* distinguish pid issuer session id and wallet backend session id in C'

Other changes:

* migrate the terminology table to a list

### Version 2.2 (August 2024)

Major changes:

* Added section on Performance, Availability
* Add detailed flow for PID Option C''
* Added credential response encryption to (Q)EAA and all PID issuing flows
* Cryptographic Algorithms: Added secp256r1 (known as NIST P-256) to the issuer
  list to increase the publicly available HSM support

Other changes:

* Cryptographic Algorithms: Aligned the sha bit size with the signature size
* Cryptographic Algorithms: Made it clearer which algorithms the verifier needs
  to use when signing the oid4vp request
* align across the flows how the information about the keys and cryptographic
  artifacts is expressed
* Re-naming dev_eph_priv/pub to pp_eph_priv/pub
* Updated example of a new seed_credential grant type
* Removed references to Batch Credential Endpoint since it was deprecated in VCI
  spec itself
* Various minor editorial fixes

*Note: This version does not yet contain the outcome of the privacy workshop.*

### Version 2.1 (July 2024)

Major changes:

* Added UX Considerations for all Options
* Added PID metadata for the SD-JWT VC format
* Cleanup of description of Option D, added step-by-step description of the
  issuance flow
* Optimization of the eID card scan from two scans to a single scan
* Add seed credential grant type extension for OpenID4VCI for PID issuance (flow
  B')
* Add supported crypto algorithms
* Defined a new credential format `seed_credential` for seed credential issuance
  to send two PoPs and pin_derived_eph_pub in the credential request and not a
  separate request.
* Changed from concatenating two parameters to using JWT claims in the payload
  when generating PoPs
* Defined a new credential format `mso_mdoc_authenticated_channel` that returns
  not just IssuerSigned, but also DeviceSigned in the credential response in
  option B and B'
* Defined how to pass rp_eph_pub in the credential request in option B and B'

Other changes:

* Added examples for requests and payloads
* Changed terms from "user_pin signed nonce" to "PoP for pin derived ephemeral
  key"; "dev signed nonce" to "PoP for dev key"; "wallet_auth signed nonce" to
  "PoP for wallet app's key"
* Clarified which key to use for DPoP key in Option B'
* Optimized User Journeys
* Changed calls that fetch a nonce from HTTP GET to HTTP POST
* Renamed pid_issuer_nonce to pid_issuer_session_id
* Improve introduction text
* Introduced term "EUDIW Provider"
* Various minor editorial fixes
* Various adaptations to align with updated specifications
* Removed usage of TLS-PSK in issuance flows
* Option B: changed to PAR to be compliant with other PID options
* Option B': removed PID PIN derived cryptographic key from the seed credential
  & added description of the seed credential
* Option C': changed Seed Credential to DPoP-bound refresh token

Tooling changes:

* Automatic PDF generation from markdown files

### Version 2 (March 2024)

New content:

* Added Options B', C' and C'' as variants of the existing options B and C
* Added section on Revocation
* Added section for PID content for SD-JWT VC
* Added new chapter [Operating Model](./02-egom.md)
* Added section [Wallet Invocation](./appendices/EUDI_Link_Governance.md)
* Added terminology section

Major changes:

* Reworked the PID option overview
* Expanded privacy considerations
* Consolidated PID Options B.1.1 and B.1.2 into a unified Option with a single
  sequence diagram

Other changes:

* Incorporated UI/UX feedback
* Various minor editorial fixes from feedback on version 1

### Version 1 (November 2023)

* Initial release
