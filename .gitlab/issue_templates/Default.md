<!---
PLEASE READ BEFORE SUBMITTING AN ISSUE

Create **a separate issue per topic**
(exception: purely editorial remarks).

Report your feedback as markdown text in the issue itself,
do not upload attachments containing comments
(like commented PDFs, ISO-style comment documents).

Please use the following template for your issue,
feel free to adapt as needed:
-->

### Type

<!-- Please select either of them:
- General
- Technical
- Editorial
-->

### Summary
<!-- Please provide a short summary of the feedback reported. -->

### Version and Section
<!-- Please provide a reference to the version of the architecture
 proposal and the specific section within the document.
 If necessary, provide a quote.
-->

### Feedback / Questions
<!-- Please provide your Feedback, proposed changes or questions here.
 Also, explain the motivation and further explanations for
 the proposed changes.
-->
