# Contributing

To give us feedback for the architecture proposal, you can open an issue in this
repository so we can discuss it. If you want to contribute to the architecture
proposal, you can open a pull request with your proposed changes.

To be able to open pull requests, you need to sign the [Contributor License
Agreement](./CLA.docx) and send it to us. After we have received the signed
document, we will grant you the required role.

## Merge requests
To guarantee a smooth review process, please follow these guidelines when
creating a merge request:

* Create a new branch for each merge request
* Commits of the merge requests are squashed into a single commit before
  merging. Make sure the squashed commit message (can be edited during the merge
  request in OpenCode) follows the [Conventional
  Commits](https://www.conventionalcommits.org/en/v1.0.0/) format. As default
  the first commit message is used.
* Each merge request should include the issue it is addressing. Write `Closes
  #<issue number>` in the description of the merge request so the issue is
  automatically closed when the merge request is merged.
* Assign a reviewer from the project team to the merge request.
* As soon as the reviewer has approved the merge request:
    * If your merge request only addresses editorial things, feel free to merge
    it.
    * For all other merge requests, or if you are unsure, assign Daniel the
    merge request. This will allow Daniel to merge the merge request knowing
    another persons has reviewed it. This can be done by the author or the
    reviewer.
* after a successful merge, delete the branch

In case of a wrong submitted commit message where the CICD pipeline fails, you
can can correct the commit message by amending the commit message and force
pushing it to the branch. This will trigger the pipeline again.

An easier alternative is to [cocogitto](https://docs.cocogitto.io/guide/edit.html#rewrite-non-compliant-commits)

* install cocogitto
* run `EDITOR=nano cocogitto edit` (you can use any other editor instead of nano)
* `git push --force`

## Run mkdocs locally

```bash
python3 -m venv venv
source venv/bin/activate
python3 -m pip install -r requirements.txt
mkdocs serve
```

In case you want to render the puml files locally, run the service as docker
container:

```bash
docker run -d -p 8080:8080 plantuml/plantuml-server:jetty
PUML_URL=http://localhost:8080/plantuml mkdocs serve
```

(For subsequent usages, `source venv/bin/activate` and `mkdocs serve` are
sufficient.)

Then open <http://127.0.0.1:8000> in your browser.

### Fix markdown linting issues

#### max line

VS Code + Rewrap Extension: The Rewrap extension for Visual Studio Code works
particularly well with Markdown lists.

When using Rewrap in VS Code:

* Highlight a block of text, including lists or indented content.
* Use the command palette (Ctrl+Shift+P or Cmd+Shift+P) and run the Rewrap
  Comment/Block command.
* The extension will automatically adjust indentation for lists and maintain
  formatting, even when wrapping lines.

#### markdownlint

Another extension called `markdownlint` can be used to check for markdown. It
will highlight the most errors, but is not able to auto fix all of them.

When nodejs is installed, you can run `npx markdownlint
architecture-proposal/**/*.md` in the terminal that will highlight all errors.
